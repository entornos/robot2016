/*
 * mpu6050_task.c
 *
 *  Created on: 17 de ene. de 2016
 *      Author: javi
 */

#include "nil.h"
#include "hal.h"

#include <string.h>
#include <math.h>
#include "bot.h"
#include "cmd_task.h"
#include "chprintf.h"
#include "mpu6050_task.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"

///////////////////////////////////////////////////////////////////////
// Constantes estáticas
///////////////////////////////////////////////////////////////////////

static BaseSequentialStream * const OUT = (BaseSequentialStream *)&SD1;


///////////////////////////////////////////////////////////////////////
// Tarea
///////////////////////////////////////////////////////////////////////

THD_FUNCTION(MpuThread, arg) {

	(void)arg;	// suppress warning

	bot_mpu_init();

	while (1)
	{
		bot_mpu_lee_inerciales();
		if (modo_test == MODO_MPU) {
			chprintf(OUT, "g %4d %4d %4d r %4d %4d %4d rg %4d\r\n",
					(int16_t)(bot_gra_x() / 1073742L),
					(int16_t)(bot_gra_y() / 1073742L),
					(int16_t)(bot_gra_z() / 1073742L),
					(int16_t)(bot_rum_x() / 1073742L),
					(int16_t)(bot_rum_y() / 1073742L),
					(int16_t)(bot_rum_z() / 1073742L),
					bot_rum_gr());
		}
		bot_ping();
		if (modo_test == MODO_SONAR) {
			chprintf(OUT, "s %u", bot_distancia());
		}
	}
}

