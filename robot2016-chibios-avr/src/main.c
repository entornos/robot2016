/*
    ChibiOS - Copyright (C) 2006..2015 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

#include <cmd_task.h>
#include <main_task.h>
#include "nil.h"
#include "hal.h"
#include "board.h"
#include "mpu6050_task.h"
#include "bot.h"
#include "chprintf.h"


/*
 * Thread working areas
 */
THD_WORKING_AREA(waMainThread, 256);
THD_WORKING_AREA(waCmdThread, 128);
THD_WORKING_AREA(waMpuThread, 512);

/*
 * Threads static table, one entry per thread.  A thread's priority is
 * determined by its position in the table with highest priority first.
 *
 * These threads start with a null argument.  A thread's name may also
 * be null to save RAM since the name is currently not used.
 *
 * The number of entries must match NIL_CFG_NUM_THREADS.
 */
THD_TABLE_BEGIN
THD_TABLE_ENTRY(waCmdThread, NULL, CmdThread, NULL)
THD_TABLE_ENTRY(waMpuThread, NULL, MpuThread, NULL)
THD_TABLE_ENTRY(waMainThread, NULL, MainThread, NULL)
THD_TABLE_END

/*
 * Application entry point.
 */
int main(void) {

	/*
	 * System initializations.
	 * - HAL initialization, this also initializes the configured device drivers
	 *   and performs the board-specific initializations.
	 * - Kernel initialization, the main() function becomes a thread and the
	 *   RTOS is active.
	 */
	halInit();

	bot_setup();

	chSysInit();

	/* This is now the idle thread loop, you may perform here a low priority
     task but you must never try to sleep or wait in this loop. Note that
     this tasks runs at the lowest priority level so any instruction added
     here will be executed after all other tasks have been started.*/
	while (true) {
	}
}


#if 0

#include <avr/io.h>
#include <avr/interrupt.h>


THD_WORKING_AREA(waMainThread, 32);
static THD_FUNCTION(MainThread, arg) {

	(void)arg;
	chRegSetThreadName("Blink");
	while (true) {
		palTogglePad(IOPORT2, PORTB_LED1);
		chThdSleepMilliseconds(1000);
	}
}



THD_TABLE_BEGIN
THD_TABLE_ENTRY(waMainThread, "blinker", MainThread, NULL)
THD_TABLE_ENTRY(waMpuThread, "hello", MpuThread, NULL)
THD_TABLE_END

/*
 * Application entry point.
 */
int main(void) {

	/*
	 * System initializations.
	 * - HAL initialization, this also initializes the configured device drivers
	 *   and performs the board-specific initializations.
	 * - Kernel initialization, the main() function becomes a thread and the
	 *   RTOS is active.
	 */
	halInit();


	chSysInit();

	palClearPad(IOPORT2, PORTB_LED1);

	/*
	 * Activates the serial driver 1 using the driver default configuration.
	 */
	sdStart(&SD1, NULL);

	/*
	 * Starts the LED blinker thread.
	 */
	chThdCreateStatic(waMainThread, sizeof(waMainThread), NORMALPRIO, MainThread, NULL);

	chprintf(&SD1, "Hello World!\r\n");
	while(TRUE) {
		chThdSleepMilliseconds(1000);
	}
}
#endif
