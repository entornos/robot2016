/*
 * task_cmd.c
 *
 *  Created on: 29 de ene. de 2016
 *      Author: ad
 */

#include <stdbool.h>

#include "cmd_task.h"
#include "nil.h"
#include "hal.h"
#include "bot.h"
#include "chprintf.h"

///////////////////////////////////////////////////////////////////////
// Constantes estáticas
///////////////////////////////////////////////////////////////////////

static BaseSequentialStream * const OUT = (BaseSequentialStream *)&SD1;

///////////////////////////////////////////////////////////////////////
// Variables exportadas
///////////////////////////////////////////////////////////////////////

enum modo_test_t modo_test = 0;

/*
 * Cmd Thread
 */

THD_FUNCTION(CmdThread, arg) {
	(void)arg;

	int vi = 0, vd = 0;

	while (true) {
		msg_t b = chnGetTimeout(&SD1, TIME_INFINITE);
		switch(b) {
		case ' ':
			bot_panico(true);
			vi = vd = 0;
			modo_test = MODO_PANICO;
			break;
		case 'i':
			chprintf(OUT, "Test mot izq+\r\n");
			bot_panico(false);
			vi = 50;
			bot_velocidad(vi, vd);
			break;
		case 'I':
			chprintf(OUT, "Test mot izq-\r\n");
			bot_panico(false);
			vi = -50;
			bot_velocidad(vi, vd);
			break;
		case 'd':
			chprintf(OUT, "Test mot der+\r\n");
			bot_panico(false);
			vd = 50;
			bot_velocidad(vi, vd);
			break;
		case 'D':
			chprintf(OUT, "Test mot der-\r\n");
			bot_panico(false);
			vd = -50;
			bot_velocidad(vi, vd);
			break;
		case 'f':
		    chprintf(OUT, "l %d\r\n", bot_lata_negra());
			break;
		case 's':
			modo_test = MODO_SONAR;
			chprintf(OUT, "Test sonar\r\n");
			break;
		case 'l':
			chprintf(OUT, "Test ledon 1\r\n");
			bot_enciende_cny70(true);
			break;
		case 'L':
			chprintf(OUT, "Test ledon 0\r\n");
			bot_enciende_cny70(false);
			break;

		case 'm':
			modo_test = MODO_MPU;
			chprintf(OUT, "Test MPU\r\n");
			break;

		case 'B':
			bot_reboot_to_bootloader();
			break;
		case 'R':
			bot_reboot_to_app();
			break;
		}
	}
}
