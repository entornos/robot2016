/*
 * task_main.c
 *
 *  Created on: 29 de ene. de 2016
 *      Author: ad
 */

#include "nil.h"
#include "hal.h"
#include "bot.h"
#include "chprintf.h"

static BaseSequentialStream * const OUT = (BaseSequentialStream *)&SD1;


/*
 * Main thread
 */
THD_FUNCTION(MainThread, arg) {
	uint8_t s[6];
	(void)arg;

	bot_enciende_cny70(true);

	while (true) {
		bot_lee_cny70(s);
		//int mm = bot_ping();
		//chprintf(OUT, "ping %d\r\n", mm);
		//bot_velocidad(30, 30);

		//uint16_t t0 = TCNT1;
		//bot_led(1);
		//bot_busy_delay(4);
		//bot_led(0);

		//chprintf(OUT, "%u\r\n", TCNT1 - t0);
		//chprintf(OUT, "%3d %3d %3d %3d %3d %3d\r\n", s[0], s[1], s[2], s[3], s[4], s[5]);
		//chThdSleep(20000);
	}
}
