#include "inv_config.h"

#ifdef EMPL_TARGET_STM32

#include <string.h>
#include "stm32_shim.h"

#include "ch.h"
#include "hal.h"
#include "chprintf.h"


int log_e(const char *fmt, ...) {
	va_list ap;
	int formatted_bytes;

	va_start(ap, fmt);
	formatted_bytes = chvprintf((BaseSequentialStream *)&SD_MPU_DEBUG, fmt, ap);
	va_end(ap);

	return formatted_bytes;
}

int log_i(const char *fmt, ...) {
	va_list ap;
	int formatted_bytes;

	va_start(ap, fmt);
	formatted_bytes = chvprintf((BaseSequentialStream *)&SD_MPU_DEBUG, fmt, ap);
	va_end(ap);

	return formatted_bytes;
}

/** Write multiple bytes to an 8-bit device register.
 * @param devAddr I2C slave device address
 * @param regAddr First register address to write to
 * @param length Number of bytes to write
 * @param data Buffer to copy new data from
 * @return Status of operation (0 = success, -1 = error)
 */
int i2c_write(uint8_t devAddr, uint8_t regAddr, uint8_t length, const uint8_t *data) {
	uint8_t mpu_txbuf[I2CDEV_BUFFER_LENGTH], mpu_rxbuf[1];
	msg_t rdymsg;
	if((length + 1)> I2CDEV_BUFFER_LENGTH) {
		log_e("ERROR readBytes: length + 1 > I2CDEV BUFFERLENGTH\n");
		return -1;
	}
	mpu_txbuf[0] = regAddr;
	memcpy(mpu_txbuf + 1, data, sizeof(uint8_t) * length);

	i2cAcquireBus(&I2C_MPU);
	rdymsg = i2cMasterTransmit(&I2C_MPU, devAddr, mpu_txbuf, length + 1, mpu_rxbuf, 0);
	i2cReleaseBus(&I2C_MPU);
	if(rdymsg == MSG_TIMEOUT || rdymsg == MSG_RESET) {
		log_e("I2C ERROR: %d\n", i2cGetErrors(&I2CD1));
		return -1;
	}
	return 0;
}

/** Read multiple bytes from an 8-bit device register.
 * @param devAddr I2C slave device address
 * @param regAddr First register regAddr to read from
 * @param length Number of bytes to read
 * @param data Buffer to store read data in
 * @return Number of bytes read (-1 indicates failure)
 */
int i2c_read(uint8_t devAddr, uint8_t regAddr, uint8_t length, uint8_t *data) {
	//uint8_t mpu_txbuf[1], mpu_rxbuf[I2CDEV_BUFFER_LENGTH], i;
	msg_t rdymsg;
	if(length > I2CDEV_BUFFER_LENGTH) {
		log_e("ERROR readBytes: length > I2CDEV BUFFERLENGTH\n");
		return -1;
	}
	i2cAcquireBus(&I2C_MPU);
	rdymsg = i2cMasterTransmitTimeout(&I2C_MPU, devAddr, &regAddr, 1, data, length, MS2ST(1000));	// milliseconds
	i2cReleaseBus(&I2C_MPU);
	if(rdymsg == MSG_TIMEOUT || rdymsg == MSG_RESET) {
		log_e("I2C ERROR: %d\n", i2cGetErrors(&I2CD1));
		return -1;
	}
	return 0;
}

int reg_int_cb(struct int_param_s *int_param)
{
//	attachInterrupt(int_param->pin, int_param->cb, RISING);
	return 0;
}

void delay_ms(unsigned long num_ms)
{
	chThdSleepMilliseconds(num_ms);
}

void get_ms(unsigned long *count) {
	*count = 0; // TODO
}

#endif // EMPL_TARGET_STM32
