#pragma once

#ifdef EMPL_TARGET_STM32

// Defines / STM32-specific config

#define I2C_MPU 				I2CD1
#define MPU_INT_PORT			GPIOB
#define MPU_INT_PIN				4
#define I2CDEV_BUFFER_LENGTH 	64
#define SD_MPU_DEBUG			SD1

// Structs
struct int_param_s {
    void (*cb)(void);
    unsigned short pin;
};

// Functions
int i2c_write(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char const *data);
int i2c_read(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char *data);
void delay_ms(unsigned long num_ms);
void get_ms(unsigned long *count);
int reg_int_cb(struct int_param_s *int_param);
#ifdef SD_MPU_DEBUG
int log_e(const char *fmt, ...);
int log_i(const char *fmt, ...);
#endif

// Macros
#define labs(x)     (((x)>0)?(x):-(x))
#define fabs(x)     (((x)>0)?(x):-(x))
#define min(x, y)	(((x) < (y)) ? (x) : (y))
#ifndef SD_MPU_DEBUG
#  define log_i(...) do { } while (0)
#  define log_e(...) do { } while (0)
#endif


#endif  // EMPL_TARGET_STM32
