#pragma once

#ifdef EMPL_TARGET_ARDUINO

// AVR-specific includes
#include <avr/pgmspace.h>

// Defines / AVR-specific config
#define I2C_MPU 				I2CD1
#define I2CDEV_BUFFER_LENGTH    64
//#define SD_MPU_DEBUG			SD1

// Functions
int i2c_write(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char const *data);
int i2c_read(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char *data);
void delay_ms(unsigned long num_ms);
void get_ms(unsigned long *count);
#ifdef SD_MPU_DEBUG
int log_e(const char *fmt, ...);
int log_i(const char *fmt, ...);
#endif


// Macros
// #define labs(x)     (((x)>0)?(x):-(x)) // already defined in avr stdlib
#define fabs(x)     (((x)>0)?(x):-(x))
#define min(x, y)	(((x) < (y)) ? (x) : (y))
#ifndef SD_MPU_DEBUG
#  define log_i(...) do { } while (0)
#  define log_e(...) do { } while (0)
#endif


#endif // defined EMPL_TARGET_ARDUINO
