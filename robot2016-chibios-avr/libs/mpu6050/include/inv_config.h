#pragma once

// Target platform: define ONLY ONE
//#define EMPL_TARGET_STM32
#define EMPL_TARGET_ARDUINO

// Chip
#define MPU6050

#include "arduino_shim.h"
#include "stm32_shim.h"
