#pragma once

#include <stdint.h>

void bot_mpu_init(void);
void bot_mpu_lee_inerciales(void);
int32_t bot_gra_x(void);
int32_t bot_gra_y(void);
int32_t bot_gra_z(void);
int32_t bot_rum_x(void);
int32_t bot_rum_y(void);
int32_t bot_rum_z(void);
int16_t bot_rum_gr(void);
