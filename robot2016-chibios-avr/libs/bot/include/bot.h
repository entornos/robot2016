#pragma once

#include <stdbool.h>

#include "bot_config.h"

#include "bot_cny70.h"
#include "bot_motor.h"
#include "bot_mpu.h"
#include "bot_reset.h"
#include "bot_setup.h"
#include "bot_sonar.h"
#include "bot_tcrt1000.h"
#include "bot_tiempo.h"
