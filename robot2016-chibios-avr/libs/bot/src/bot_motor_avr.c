#include "bot.h"
#if BOT_PLATFORM == 2015

#include <avr/io.h>

///////////////////////////////////////////////////////////////////////
// Funciones privadas-exportadas
///////////////////////////////////////////////////////////////////////

void
bot_motor_1(int32_t v)
{
	if (v >= 0) {
		if (v > 255)
			v = 255;
		OCR0A = v;        // PWM invertido en una pata
		OCR0B = 0;        // y deja en alto la otra
	}
	else {              // Contramarcha
		if (v < -255)
			v = -255;
		OCR0B = -v;
		OCR0A = 0;
	}
}

void
bot_motor_2(int32_t v)
{
	if (v > 0) {
		if (v > 255)
			v = 255;
		OCR2B = v;
		OCR2A = 0;
	}
	else {
		if (v < -255)
			v = -255;
		OCR2A = -v;
		OCR2B = 0;
	}
}

#endif
