#include <stdint.h>

///////////////////////////////////////////////////////////////////////
// Constantes privadas
///////////////////////////////////////////////////////////////////////

const static int UMBRAL_ACE = 50;   // umbral movimiento acelerometro (ojo, esto varia segun la precision)
const static int UMBRAL_GIR = 40;   // umbral movimiento giróscopo
const static int FRECUENCIA = 100;  // frecuencia de muestreo (Hz)

///////////////////////////////////////////////////////////////////////
// Variables privadas
///////////////////////////////////////////////////////////////////////

static int32_t dis[3];        // distancia recorrida
static uint32_t reposo[3];         // tiempo en reposo
static int32_t ang[3];        // angulo girado (en unidades del giroscopo)

///////////////////////////////////////////////////////////////////////
// Variables exportadas
///////////////////////////////////////////////////////////////////////

int16_t ace[3];        // vector aceleracion
int16_t gra[3];        // vector gravedad
int32_t vel[3];        // vector velocidad (en unidades del acelerometro)
int16_t velmm[3];      // vector velocidad (en mm/s)
int16_t dismm[3];      // distancia recorrida (en mm)

///////////////////////////////////////////////////////////////////////
// Funciones exportadas
///////////////////////////////////////////////////////////////////////

void bot_odo_integra_int(void)
{
  static int16_t ace0[3] = { 0, 0, 0 }, velmm0[3] = { 0, 0, 0};

  // Esta función tarda 380 us en ejecutarse (asumiendo NO reposo)
  for (int i = 0; i < 3; i++) {    // componentes x, y, z
    // calcula gravedad
    // gra[i] = ALFA * gra[i] + (1 - ALFA) * ace[i];
    // compensa gravedad
    // ace[i] -= gra[i];

    // integra la aceleracion -> velocidad, solo si la aceleracion es significativa
    if (ace[i] > UMBRAL_ACE || ace[i] <= -UMBRAL_ACE) {
      vel[i] += (ace0[i] + ace[i]) / 2;
      velmm[i] = vel[i] * 9810L / 2048L / FRECUENCIA;
      reposo[i] = 0;
    } else {
      // Despues de 0,125 segundos sin aceleracion, asume reposo (resetea las velocidades)
      if (++reposo[i] > FRECUENCIA / 8) //  && vi == 0 && vd == 0)
        vel[i] = velmm[i] = 0;
    }

    // integra la velocidad -> distancia
    dis[i] += (velmm0[i] + velmm[i]) / 2;
    dismm[i] = dis[i] / FRECUENCIA;


    ace0[i] = ace[i];
    velmm0[i] = velmm[i];
  }
}
