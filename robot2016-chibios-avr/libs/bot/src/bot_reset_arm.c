#include "bot.h"
#if BOT_PLATFORM == 2016

#include "hal.h"

// TODO: sale I2C ERROR al reiniciar
void bot_reboot_to_bootloader(void)
{
	NVIC_SystemReset();
}

void bot_reboot_to_app(void)
{
	NVIC_SystemReset();
}

#endif
