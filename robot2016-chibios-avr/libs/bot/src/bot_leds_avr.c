#include "bot.h"
#if BOT_PLATFORM == 2015

#include <avr/io.h>


void bot_led(bool estado) {
	if (estado) {
		//palSetPad(IOPORT2, PORTB_LED1);
		PORTB |= _BV(PINB5);
	} else {
		//palClearPad(IOPORT2, PORTB_LED1);
		PORTB &= ~_BV(PINB5);
	}
}

#endif
