#include "bot.h"
#if BOT_PLATFORM == 2015

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

/** Reboot to bootloader.
 * Sets a 15 ms watchdog timer and idles until the watchdog reboots
 * the AVR. Registers r2 = r3 = 0 are used to signal bootloader run.
 */
void __attribute__((noreturn)) bot_reboot_to_bootloader(void) {
	wdt_enable(WDTO_120MS);
	asm("eor r1, r1");
	asm("mov r2, r1");
	asm("mov r3, r1");
	cli();
	sleep_cpu();
	for (;;);
}

/** Reboot to user app.
 * Sets a 15 ms watchdog timer and idles until the watchdog reboots
 * the AVR. Registers r2/r3 = 0xb0aa are used to signal app run.
 */
void __attribute__((noreturn)) bot_reboot_to_app(void) {
	wdt_enable(WDTO_120MS);
	cli();
	asm("ldi r20, 0xb0");
	asm("mov r2, r20");
	asm("ldi r20, 0xaa");
	asm("mov r3, r20");
	sleep_cpu();
	for (;;);
}

#endif
