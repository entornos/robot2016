#include "bot.h"
#if BOT_PLATFORM == 2016

#include "ch.h"
#include "hal.h"

///////////////////////////////////////////////////////////////////////
// Enums
///////////////////////////////////////////////////////////////////////

enum estado_sonar_t {
		SONAR_OFF,
		WAIT_ECHO_CLEAR,
		WAIT_PING_START,
		WAIT_PING_END
};

///////////////////////////////////////////////////////////////////////
// Variables estáticas
///////////////////////////////////////////////////////////////////////

static enum estado_sonar_t estado_sonar;
static thread_reference_t sonar_trp = NULL;
static int ping_t0;


///////////////////////////////////////////////////////////////////////
// ISRs (exportadas)
///////////////////////////////////////////////////////////////////////

void
sonar_isr(EXTDriver *extp, expchannel_t channel)
{
	(void)extp;		// suppress gcc warnings
	(void)channel;

	chSysLockFromISR();

	switch(estado_sonar) {
	case SONAR_OFF:
		break;
	case WAIT_ECHO_CLEAR:
		if (! palReadPad(GPIOB, 15))  // espera 0
			estado_sonar = WAIT_PING_START;
		break;

	case WAIT_PING_START:
		if (palReadPad(GPIOB, 15)) {   // espera 1 (empieza a contar tiempo)
			estado_sonar = WAIT_PING_END;
			ping_t0 = gptGetCounterX(&GPTD1);
		}
		break;

	case WAIT_PING_END:
		if (!palReadPad(GPIOB, 15)) {  // espera 0 (fin medición)
			estado_sonar = SONAR_OFF;
			int ping_time = gptGetCounterX(&GPTD1) - ping_t0;

			/* Wakes up the thread.*/
			chThdResumeI(&sonar_trp, ping_time);  /* Resuming the thread with message.*/
		}
		break;
	}

	chSysUnlockFromISR();
}


///////////////////////////////////////////////////////////////////////
// Functiones estáticas
///////////////////////////////////////////////////////////////////////

static inline void delay_until_us(int32_t start, int32_t us) {
	int32_t elapsed;
	int32_t ticks = us * (STM32_HCLK / 1000000);
	//chSysLock();
	do {
		elapsed = start - SysTick->VAL;
		if (elapsed < 0) elapsed += (STM32_HCLK / OSAL_ST_FREQUENCY);
	} while (elapsed < ticks);
	//chSysUnlock();
}


///////////////////////////////////////////////////////////////////////
// Functiones exportadas
///////////////////////////////////////////////////////////////////////

int
bot_sonar_ping(void)
{
	int es;
	chSysLock();	// desactiva todas las IRQ pero no las FIQ
	es = estado_sonar;
	chSysUnlock();

	if (es == SONAR_OFF) {
		// TODO: in board.h set it to pushpull, low
		palClearPad(GPIOB, 15);			// Pin to low (should already be)
		palSetPadMode(GPIOB, 15, PAL_MODE_OUTPUT_PUSHPULL);	// Pin to output
		chSysLock();					// CLI
		gptPolledDelay(&GPTD1, 4);		// Wait for pin to go low, testing show 4us is ok
		palSetPad(GPIOB, 15);			// Trigger pin high, tells the sensor to ping
		gptPolledDelay(&GPTD1, 10);		// Wait 10us for the sensor to ack the trigger
		estado_sonar = WAIT_ECHO_CLEAR;
		palClearPad(GPIOB, 15);			// Trigger pin low
		gptStartContinuousI(&GPTD1, 65536);		// Start timer 1. I-class call because it croaks with SV#4 otherwise (double lock?)
		palSetPadMode(GPIOB, 15, PAL_MODE_INPUT);			// Pin to input

		/* Wait for the IRQ to happen.*/
		// chSysLock();	// we're still CLI
		int ping_time = chThdSuspendTimeoutS(&sonar_trp, TIME_INFINITE);
		chSysUnlock();

		gptStopTimer(&GPTD1);

		int mm = ping_time * 11 / 64;
		return mm;
	}
	else {
		return 0;
	}
}


#endif

