#include <stdint.h>
#include "nil.h"
#include "hal.h"

extern volatile uint16_t ping_time;

uint16_t
bot_distancia(void)
{
	chSysLock();
	uint16_t t = ping_time;
	chSysUnlock();
	return t * 11 / 16;		// TODO: asume 1 t = 4 us
}
