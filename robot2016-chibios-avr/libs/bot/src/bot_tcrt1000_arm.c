#include "bot.h"
#if BOT_PLATFORM == 2016

#include "ch.h"
#include "hal.h"

bool bot_lata_negra(void)
{
	return palReadPad(GPIOA, 7) ? 1 : 0;
}

#endif
