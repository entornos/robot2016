#include "bot.h"
#if BOT_PLATFORM == 2015

#include "nil.h"
#include "hal.h"

///////////////////////////////////////////////////////////////////////
// Variables externas
///////////////////////////////////////////////////////////////////////

extern thread_reference_t mpu_trp;


///////////////////////////////////////////////////////////////////////
// ISRs
///////////////////////////////////////////////////////////////////////

// https://sites.google.com/site/qeewiki/books/avr-guide/external-interrupts-on-the-atmega328
OSAL_IRQ_HANDLER(INT0_vect) {

	OSAL_IRQ_PROLOGUE();

	/* Wakes up the thread.*/
	osalSysLockFromISR();
	chThdResumeI(&mpu_trp, (msg_t)0x1337);
	osalSysUnlockFromISR();

	OSAL_IRQ_EPILOGUE();
}

#endif
