#pragma once

#include "hal.h"

enum modo_test_t {
	MODO_PANICO,
	MODO_SENSORES_FRONTALES,
	MODO_MPU,
	MODO_SONAR
};

extern enum modo_test_t modo_test;

THD_FUNCTION(CmdThread, arg);

