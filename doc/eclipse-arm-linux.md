# Instalar Eclipse #

Descargar de http://www.eclipse.org/downloads -> Eclipse IDE for C/C++ Developers

Crear en el escritorio una carpeta 'ROBOT' donde meteremos todo.

![pantalla](img/eclipse-arm-linux-01.png)

En Linux Mint 17 (Ubuntu 14.4) el eclipse a veces se redibuja mal. Para arreglarlo, crear el lanzador `~/.local/share/applications/eclipse.desktop`, meter esto dentro y siempre arrancaremos eclipse desde este lanzador (menú de inicio).

```
[Desktop Entry]
Version=4.4
Name=Eclipse
Comment=Eclipse IDE
Exec=env UBUNTU_MENUPROXY=0 SWT_GTK3=0 /home/ad/Desktop/ROBOT/eclipse/eclipse
Icon=/home/ad/Desktop/ROBOT/eclipse/icon.xpm
Terminal=false
Type=Application
Categories=Utility;Application
```

En la carpeta ROBOT, crear una carpeta "workspace" y apuntar al eclipse a esa carpeta cuando nos pregunte:

![pantalla](img/eclipse-arm-linux-02.png)

Cerrar la pestaña "Welcome"

# Instalar gcc-arm + openocd #

Gcc-arm es el compilador GCC para arquitecturas ARM, y OpenOCD es el interfaz con el programador ST-Link, permite subir código al chip ARM y debuggearlo. Lo necesitamos todo. Para instalar las dos cosas, en el terminal:

```
sudo apt-get install gcc-arm-none-eabi libnewlib-arm-none-eabi openocd
```

# Instalar plugin gnu arm de Eclipse #

Ir a Eclipse -> Help -> Eclipse Marketplace, buscar "gnu arm", e instalar el plugin "GNU ARM Eclipse".

Dejar las opciones como están:

![](img/eclipse-arm-linux-03.png)

Decir a todo que sí (aceptar la licencia, confirmar instalar software sin firmar, etc). Al final, reiniciar eclipse.

