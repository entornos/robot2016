# Control de versiones #

Usaremos git para mantener el código versionado. Periódicamente haremos "commits", que equivalen a hacer copias de todo el código a las cuales podemos volver más tarde. Git mantiene el histórico de cambios en local, pero también lo podemos subir a http://bitbucket.org y tener una copia en Internet (por seguridad, para trabajar en equipo, etc.)

### Fork inicial ###

Lo primero que necesitamos es una cuenta en http://bitbucket.org -> Get Started. Metemos dirección de correo y lo típico para registrarnos en cualquier lado.

Luego haremos un "Fork" del repositorio esqueleto yendo a:

![](img/eclipse-git-01.png)

Cuando esté hecho el fork, copiamos la dirección a nuestro repositorio, que será parecido a esto pero en vez de "entornos" pondrá vuestro usuario:

![](img/eclipse-git-02.png)

### Clonar el repositorio en local ###

Lo siguiente es bajarnos una copia local del repositorio clonado.

En eclipse -> File -> Import -> Git -> Projects from Git -> Clone URI.

En URI pegamos la dirección que hemos copiado en Bitbucket, y en Password la clave.

![](img/eclipse-git-03.png)

Si marcamos "Store in Secure Store", guardará la clave en un almacén de claves y nos pedirá una nueva clave para proteger el almacén. Opcional pero recomendado.

Nos preguntará qué ramas clonar, dejarlo como está (master):

![](img/eclipse-git-04.png)

Destino (MUY IMPORTANTE). Vamos a crear una carpeta en el escritorio/ROBOT llamada "git". Le damos a Browse, vamos a la carpeta git y en Name dejamos "robot2016":

![](img/eclipse-git-05.png)

Siguiente...

![](img/eclipse-git-06.png)

Proyectos a importar. Marcamos sólo estos 2:

![](img/eclipse-git-07.png)

Ya deberían aparecer los 2 proyectos en la pestaña "Project Explorer". Cerramos el del AVR (botón derecho en la raíz del proyecto -> Close Project) y dejamos abierto el de ARM. Si no, eclipse intentará compilar los dos cada vez.

![](img/eclipse-git-08.png)

Por último conviene desactivar las herramientas de análisis estático de código (intentan buscar errores comunes antes de compilar pero en realidad molestan): botón derecho en la raíz del proyecto -> Properties -> C/C++ General -> Code Analysis -> Use project settings y desactivamos todo:

![](img/eclipse-git-09.png)

Para compilar: CTRL + B, y hay que fijarse en la pestaña "Problems" por si hay errores, o mejor, en "Console":

![](img/eclipse-git-10.png)

### Hacer commits ###

Es importante hacer commits FRECUENTES. Cuanto más frecuentes mejor. Así, si algo deja de funcionar siempre podemos volver atrás justo al commit en que funcionaba y comparar para ver qué hemos roto, etc.

Para commitear (hay quien dice anotar, subir al git, submitear, submit): botón derecho en el proyecto -> Team -> Commit. Y en el "Commit message" pondremos una descripción que resuma lo que hemos cambiado desde el commit anterior. Ejemplos de mensajes ÚTILES:

 * Funciona el siguelíneas, aunque se sale por la derecha
 * Versión estable para el concurso
 * Añadida función gira
 * Constantes PD optimizadas, ver tiempos en tiempos.ods

 Ejemplo de mensajes INÚTILES:

  * Arreglado bug _(¿¿qué bug??)_
  * Ya va _(¿¿ya va el qué??)_

Abajo seleccionamos los ficheros a subir.

Si le damos a "Commit" subirá los cambios a nuestro repositorio local. Si le damos a "Commit & Push", además los subirá a bitbucket.

![](img/eclipse-git-10.png)

### Bajar la última versión del esqueleto ###

Si hago algún cambio en el esqueleto (arreglar bugs, etc) podéis bajaros la última versión haciendo un "git pull" del repositorio original. Para ello:

Primero hay que commitear todos los cambios pendientes.

Luego, desde el terminal vamos a la carpeta donde está el repositorio y hacemos el git pull:

```
cd ~/Desktop/ROBOT/git/robot2016
git pull https://bitbucket.org/entornos/robot2016.git
```

