/*
 * main_task.c
 *
 *  Created on: 23 de ene. de 2016
 *      Author: javi
 */

/*
 * mpu6050_task.c
 *
 *  Created on: 17 de ene. de 2016
 *      Author: javi
 */

#include "bot.h"
#include "cmd_task.h"
#include "ch.h"
#include "chprintf.h"
#include "hal.h"

///////////////////////////////////////////////////////////////////////
// Constantes estáticas
///////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
// Constantes exportadas
///////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
// Variables estáticas
///////////////////////////////////////////////////////////////////////

static BaseSequentialStream *OUT = (BaseSequentialStream *) &SD1;

static THD_WORKING_AREA(waMainThread, 2048);
// static thread_reference_t trp = NULL;


///////////////////////////////////////////////////////////////////////
// Variables exportadas
///////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
// Functiones estáticas
///////////////////////////////////////////////////////////////////////

static inline void delay_until_us(int32_t start, int32_t us) {
	int32_t elapsed;
	int32_t ticks = us * (STM32_HCLK / 1000000);
	//chSysLock();
	do {
		elapsed = start - SysTick->VAL;
		if (elapsed < 0) elapsed += (STM32_HCLK / OSAL_ST_FREQUENCY);
	} while (elapsed < ticks);
	//chSysUnlock();
}

static void caracteriza_motores(void)
{
	/*
	 * Encoder / wheel / motor data:
	 * - 12 counts per motor revolution
	 * - 1 wheel rev per 29.86 motor revs
	 * - PI * 32 mm per wheel rev.
	 *
	 * 12 * 29.86 encoder counts --- 32 * PI mm
	 * 1 encoder count           --- x mm       --> x = 0.28056196951 mm
	 */

	int16_t v = -255;			// velocidad (en unidades PWM)
	int16_t ti1, ti0, td1, td0;	// lecturas encoders (en unidades de encoder)
	int32_t di, dd;				// distancias (en unidades del encoder)
	int32_t vi, vd;				// velocidades (en mm/s)
	uint32_t t0;				// tiempo (en microsegundos)

	chprintf(OUT, "v (pwm),vi (mm/s),vd (mm/s)\r\n");

	bot_velocidad(v, v);
	chThdSleepMilliseconds(500);
	do {
		bot_velocidad(v, v);
		chprintf(OUT, "%d,", v);
		chThdSleepMilliseconds(100);
		t0 = bot_micros();
		ti0 = bot_encoder_izdo();
		td0 = bot_encoder_dcho();
		di = dd = 0;

		while (bot_micros() - t0 < 100000) {
			ti1 = bot_encoder_izdo();
			td1 = bot_encoder_dcho();
			di += (int16_t)(ti1 - ti0);
			dd += (int16_t)(td1 - td0);
			ti0 = ti1;
			td0 = td1;
		}
		vi = (double)di * 2.8056196951 + 0.5;
		vd = (double)dd * 2.8056196951 + 0.5;

		chprintf(OUT, "%d,%d\r\n", vi, vd);

		v += 5;
	} while (v <= 255);
	bot_velocidad(0, 0);
}

static void cabecea(void)
{
	const int32_t vmax = 40, vmin = 30;
	int16_t ti0, td0, dti, dtd;
	ti0 = bot_encoder_izdo();
	td0 = bot_encoder_dcho();

	chprintf(OUT, "cab on\r\n");
	while (modo_test == MODO_CABECEA) {
		do {
			dti = bot_encoder_izdo() - ti0;
			dtd = bot_encoder_dcho() - td0;
			if (dti > -dtd) {
				bot_velocidad(vmin, -vmax);
			} else {
				bot_velocidad(vmax, -vmin);
			}
			chThdSleepMilliseconds(10);
			bot_velocidad(0, 0);
			chThdSleepMilliseconds(30);
		} while (dti < 100 && dtd > -100);

		do {
			dti = bot_encoder_izdo() - ti0;
			dtd = bot_encoder_dcho() - td0;
			if (dti > -dtd) {
				bot_velocidad(-vmax, vmin);
			} else {
				bot_velocidad(-vmin, vmax);
			}
			chThdSleepMilliseconds(10);
			bot_velocidad(0, 0);
			chThdSleepMilliseconds(30);

		} while (dti > -100 && dtd < 100);
	}
	chprintf(OUT, "cab off\r\n");
}

static void test_micros(void)
{
	uint32_t t0, t1, tm1 = 0, stval;
	chprintf(OUT, "Test micros\r\n");
	t0 = bot_micros();
	while (1) {
		t1 = bot_micros();
		int32_t dt = t1 - t0;
		stval = SysTick->VAL;
		if (dt < 0) {
			chprintf(OUT, "tm1=%u t0=%u t1=%u st=%u\r\n", tm1, t0, t1, stval);
		}
		tm1 = t0;
		t0 = t1;
	}
}

static __attribute__((noreturn)) THD_FUNCTION(MainThread, arg) {
	(void)arg;
	uint8_t s[6];
	uint8_t sf[2];

	// enciende el led de la placa
	bot_led_placa(true);

	// empieza con los CNY70 y los TCRT1000 apagados
    bot_enciende_cny70(false);

    // y los motores apagados también
    bot_motor_standby(false);

	while (1) {

		//chprintf(OUT, "%d\r\n", bot_micros());
		switch (modo_test) {
		case MODO_MICROS:
			test_micros();
			break;
		case MODO_SENSORES_FRONTALES:
			bot_lee_tcrt1000(sf);
		    chprintf(OUT, "l %d sf %3d %3d\r\n", bot_lata_negra(), sf[0], sf[1]);
		    break;
		case MODO_ENCODERS:
			chprintf(OUT, "i %d d %d\r\n", TIM3->CNT, TIM4->CNT);
			break;
		case MODO_CNY70:
			bot_lee_cny70(s);
			chprintf(OUT, "%3d %3d %3d %3d %3d %3d\r\n",
					s[0], s[1], s[2], s[3], s[4], s[5]);
			break;
		case MODO_CARACTERIZAR_MOTORES:
			caracteriza_motores();
			modo_test = MODO_PANICO;
			break;
#if 0
		case MODO_GIRA_1:
			gira(16384, 0); // adelante horario
			modo_test = MODO_PANICO;
			break;
		case MODO_GIRA_2:
			gira(-16384, 0); // atrás antihorario
			modo_test = MODO_PANICO;
			break;
		case MODO_GIRA_3:
			gira(16384, -45); // atrás horario
			modo_test = MODO_PANICO;
			break;
		case MODO_GIRA_4:
			gira(-16384, -45); // adelante antihorario
			modo_test = MODO_PANICO;
			break;
#endif
		case MODO_CABECEA:
			cabecea();
			break;
		default:
			break;
		}
	}
}

///////////////////////////////////////////////////////////////////////
// Funciones exportadas
///////////////////////////////////////////////////////////////////////

void
main_task_create(tprio_t prio)
{
	chThdCreateStatic(waMainThread, sizeof(waMainThread), prio, MainThread, NULL);
}
