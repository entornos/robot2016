/*
 * serial_task.c
 *
 *  Created on: 23 de ene. de 2016
 *      Author: javi
 */

#include "bot.h"
#include "cmd_task.h"
#include "ch.h"
#include "chprintf.h"
#include "hal.h"



///////////////////////////////////////////////////////////////////////
// Constantes estáticas
///////////////////////////////////////////////////////////////////////

static BaseSequentialStream * const OUT = (BaseSequentialStream *)&SD1;

static volatile uint32_t bootloader_flag __attribute__((section (".ram0")));

///////////////////////////////////////////////////////////////////////
// Constantes exportadas
///////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
// Variables estáticas
///////////////////////////////////////////////////////////////////////

THD_WORKING_AREA(waCmdThread, 256);
//static thread_reference_t trp = NULL;

///////////////////////////////////////////////////////////////////////
// Variables exportadas
///////////////////////////////////////////////////////////////////////

enum modo_test_t modo_test = 0;

///////////////////////////////////////////////////////////////////////
// Variables importadas
///////////////////////////////////////////////////////////////////////

extern int32_t nkpg;
extern int32_t vgmax;
extern int32_t kdg;

///////////////////////////////////////////////////////////////////////
// Functiones estáticas
///////////////////////////////////////////////////////////////////////

static /* __attribute__((noreturn)) */ THD_FUNCTION(CmdThread, arg) {
	(void)arg;

	int vi = 0, vd = 0;

	while (true) {
		msg_t b = chnGetTimeout(&SD1, TIME_INFINITE);
		switch(b) {
		////////// BEGIN NO TOCAR //////////
		case ' ':
			modo_test = MODO_PANICO;
			vi = vd = 0;
			bot_panico(true);
			break;
		case 'B':
			bot_reboot_to_bootloader();
			break;
		case 'R':
			bot_reboot_to_app();
			break;
		////////// END NO TOCAR //////////

		case 'e':
			modo_test = MODO_ENCODERS;
			break;
		case 'i':
			chprintf(OUT, "Test mot izq+\r\n");
			bot_panico(false);
			vi = 50;
			bot_velocidad(vi, vd);
			break;
		case 'I':
			chprintf(OUT, "Test mot izq-\r\n");
			bot_panico(false);
			vi = -50;
			bot_velocidad(vi, vd);
			break;
		case 'd':
			chprintf(OUT, "Test mot der+\r\n");
			bot_panico(false);
			vd = 50;
			bot_velocidad(vi, vd);
			break;
		case 'D':
			chprintf(OUT, "Test mot der-\r\n");
			bot_panico(false);
			vd = -50;
			bot_velocidad(vi, vd);
			break;
		case 'f':
			modo_test = MODO_SENSORES_FRONTALES;
			chprintf(OUT, "Test senf\r\n");
			break;
		case 's':
			modo_test = MODO_SONAR;
			chprintf(OUT, "Test sonar\r\n");
			break;
		case 'l':
			chprintf(OUT, "Test ledon 1\r\n");
			bot_enciende_cny70(true);
			break;
		case 'L':
			chprintf(OUT, "Test ledon 0\r\n");
			bot_enciende_cny70(false);
			break;

		case 'k':
			chprintf(OUT, "3 leds on\r\n");
			bot_led_placa(1);
			bot_led_1(1);
			bot_led_2(1);
			break;

		case 'K':
			chprintf(OUT, "2 leds off\r\n");
			bot_led_placa(0);
			bot_led_1(0);
			bot_led_2(0);
			break;

		case 'm':
			modo_test = MODO_MPU;
			chprintf(OUT, "Test MPU\r\n");
			break;

		case 'c':
			modo_test = MODO_CNY70;
			chprintf(OUT, "Test CNY70\r\n");
			break;

		case 'r':
			chprintf(OUT, "RCC->CSR: %x bf=%x\r\n", RCC->CSR, bootloader_flag);
			break;

		case 'o':
			chprintf(OUT, "Mot stby 1\r\n");
			bot_motor_standby(true);
			break;
		case 'O':
			chprintf(OUT, "Mot stby 0\r\n");
			bot_motor_standby(false);
			break;

		case 'C':	// caracterizar motores
			modo_test = MODO_CARACTERIZAR_MOTORES;
			break;
		case 'M':
			modo_test = MODO_MICROS;
			break;
#if 0
		case '1':
			bot_panico(false);
			modo_test = MODO_GIRA_1;
			break;
		case '2':
			bot_panico(false);
			modo_test = MODO_GIRA_2;
			break;
		case '3':
			bot_panico(false);
			modo_test = MODO_GIRA_3;
			break;
		case '4':
			bot_panico(false);
			modo_test = MODO_GIRA_4;
			break;
		case '5':
			vgmax += 5;
			chprintf(OUT, "vgmax %d\r\n", vgmax);
			break;
		case '%':
			vgmax -= 5;
			chprintf(OUT, "vgmax %d\r\n", vgmax);
			break;
		case '6':
			nkpg += 1;
			chprintf(OUT, "kpg %d\r\n", nkpg);
			break;
		case '&':
			nkpg -= 1;
			chprintf(OUT, "kpg %d\r\n", nkpg);
			break;
		case '7':
			kdg += 100;
			chprintf(OUT, "kdg %d\r\n", kdg);
			break;
		case '/':
			kdg -= 100;
			chprintf(OUT, "kdg %d\r\n", kdg);
			break;
#endif
		case 'q':
			bot_panico(false);
			modo_test = MODO_CABECEA;
			break;
		}
		chEvtBroadcastFlags(&bot_event_source, EVT_TECLA);
	}
}


///////////////////////////////////////////////////////////////////////
// Funciones exportadas
///////////////////////////////////////////////////////////////////////

void
cmd_task_create(tprio_t prio)
{
	chThdCreateStatic(waCmdThread, sizeof(waCmdThread), prio, CmdThread, NULL);
}

