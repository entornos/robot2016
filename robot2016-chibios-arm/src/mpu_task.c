/*
 * mpu6050_task.c
 *
 *  Created on: 17 de ene. de 2016
 *      Author: javi
 */

#include <string.h>

#include "bot.h"
#include "ch.h"
#include "chprintf.h"
#include "cmd_task.h"
#include "hal.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"
#include "mpu_task.h"

///////////////////////////////////////////////////////////////////////
// Constantes estáticas
///////////////////////////////////////////////////////////////////////

static BaseSequentialStream * const OUT = (BaseSequentialStream *)&SD1;


///////////////////////////////////////////////////////////////////////
// Constantes exportadas
///////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////
// Variables estáticas
///////////////////////////////////////////////////////////////////////

static THD_WORKING_AREA(waMpuThread, 2048);

///////////////////////////////////////////////////////////////////////
// Variables exportadas
///////////////////////////////////////////////////////////////////////

thread_t *mpu_thread_p;

///////////////////////////////////////////////////////////////////////
// Functiones estáticas
///////////////////////////////////////////////////////////////////////

static THD_FUNCTION(MpuThread, arg) {

	(void)arg;	// suppress warning

	bot_mpu_init();
	chEvtObjectInit(&bot_event_source);

	while (! chThdShouldTerminateX())
	{
		bot_mpu_lee_inerciales();

		/*
		 *  Notifica a los listeners de que hay nuevos datos.
		 *
		 *  Para recibir eventos necesitamos una variable de tipo event_listener_t,
		 *  registrarla y luego esperar los eventos con chEvtWaitOne. Ejemplo:
		 *
		 *  event_listener_t escuchador;
		 *
		 *  // Registra el thread para recibir eventos del MPU
		 *  chEvtRegisterMask(&bot_event_source, &escuchador, EVT_MPU);
		 *
		 *  while (...) {
		 *    chEvtWaitOne(EVT_MPU);	// duerme hasta que el MPU tenga nuevos datos
		 *    ...
		 *  }
		 *
		 *  // Cuando ya no necesitemos recibir más eventos nos des-registramos
		 *  chEvtUnregister(&bot_event_source, &escuchador);
		 */
		chEvtBroadcastFlags(&bot_event_source, EVT_MPU);

		if (modo_test == MODO_MPU) {
			chprintf(OUT, "g %4d %4d %4d r %4d %4d %4d rg %4d\r\n",
					(int16_t)(bot_gra_x() / 1073742L),
					(int16_t)(bot_gra_y() / 1073742L),
					(int16_t)(bot_gra_z() / 1073742L),
					(int16_t)(bot_rum_x() / 1073742L),
					(int16_t)(bot_rum_y() / 1073742L),
					(int16_t)(bot_rum_z() / 1073742L),
					bot_rum_gr());
		}
	}
}


///////////////////////////////////////////////////////////////////////
// Funciones exportadas
///////////////////////////////////////////////////////////////////////

void
mpu6050_task_create(tprio_t prio)
{
	mpu_thread_p = chThdCreateStatic(waMpuThread, sizeof(waMpuThread), prio, MpuThread, NULL);
}
