#pragma once

#include "ch.h"

enum modo_test_t {
	MODO_PANICO,
	MODO_SENSORES_FRONTALES,
	MODO_MPU,
	MODO_SONAR,
	MODO_ENCODERS,
	MODO_CNY70,
	MODO_CARACTERIZAR_MOTORES,
	MODO_MICROS,
	MODO_GIRA_1,
	MODO_GIRA_2,
	MODO_GIRA_3,
	MODO_GIRA_4,
	MODO_CABECEA,
};

extern enum modo_test_t modo_test;

void cmd_task_create(tprio_t prio);

