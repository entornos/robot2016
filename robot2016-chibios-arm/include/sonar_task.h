#pragma once

#include "hal.h"

extern thread_t *sonar_thread_p;

void sonar_task_create(tprio_t prio);
void sonar_isr(EXTDriver *extp, expchannel_t channel);
