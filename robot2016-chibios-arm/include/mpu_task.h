/*
 * mpu6050_task.h
 *
 *  Created on: 17 de ene. de 2016
 *      Author: javi
 */

#pragma once

#include "hal.h"
#include "ch.h"

extern thread_t *mpu_thread_p;

void mpu6050_task_create(tprio_t prio);
void mpu6050_isr(EXTDriver *extp, expchannel_t channel);
