#pragma once

#include <stdbool.h>
#include <stdint.h>

void bot_motor_setup(void);

void bot_panico(bool p);
void bot_velocidad(int izda, int dcha);
void bot_motor_standby(bool s);
int16_t bot_encoder_izdo(void);
int16_t bot_encoder_dcho(void);


