#pragma once

#include "hal.h"
#include "ch.h"

extern mutex_t bot_sonar_mutex;

int bot_sonar_ping(void);
void bot_grafica_sonar(int mm);
void bot_datos_sonar(void);
int32_t bot_sonar_valor_eficaz_a(int32_t mm);

