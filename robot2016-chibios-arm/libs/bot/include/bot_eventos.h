#pragma once

#include "ch.h"
#include "hal.h"

#define EVT_MPU 	EVENT_MASK(0)
#define EVT_TECLA	EVENT_MASK(1)

extern event_source_t bot_event_source;
