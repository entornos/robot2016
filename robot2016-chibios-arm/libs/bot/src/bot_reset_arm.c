#include "bot.h"
#if BOT_PLATFORM == 2016

#include "hal.h"
#include "mpu_task.h"
#include "sonar_task.h"

#define BOOTAPP_SIG_1               0xdeadcafe    // boot into app signature
#define BOOT_FLAG_PTR ((uint32_t *)0x20000000)

///////////////////////////////////////////////////////////////////////
// Funciones exportadas
///////////////////////////////////////////////////////////////////////

static void reboot(void)
{
	// Es hora de morir
	chThdTerminate(mpu_thread_p);
	chThdTerminate(sonar_thread_p);

	// Espera a que los threads terminen limpiamente
	chThdWait(mpu_thread_p);
	chThdWait(sonar_thread_p);

	NVIC_SystemReset();
}

///////////////////////////////////////////////////////////////////////
// Funciones exportadas
///////////////////////////////////////////////////////////////////////

void bot_reboot_to_bootloader(void)
{
	reboot();
}

void bot_reboot_to_app(void)
{
	*BOOT_FLAG_PTR = BOOTAPP_SIG_1;
	reboot();
}

#endif
