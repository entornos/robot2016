#include "bot.h"
#if BOT_PLATFORM == 2016

#include "hal.h"
#include "ch.h"

///////////////////////////////////////////////////////////////////////
// Funciones privadas-exportadas
///////////////////////////////////////////////////////////////////////

void
bot_motor_1(int32_t v)
{
	if (v >= 0) {
		if (v > 256)
			v = 256;
		pwmEnableChannel(&PWMD2, 2, 256);
		pwmEnableChannel(&PWMD2, 3, 256 - v);
	}
	else {
		if (v < -256)
			v = -256;
		pwmEnableChannel(&PWMD2, 2, 256 + v);
		pwmEnableChannel(&PWMD2, 3, 256);
	}
}

void
bot_motor_2(int32_t v)
{
	if (v >= 0) {
		if (v > 256)
			v = 256;
		pwmEnableChannel(&PWMD2, 0, 256);
		pwmEnableChannel(&PWMD2, 1, 256 - v);
	}
	else {              // Contramarcha
		if (v < -256)
			v = -256;
		pwmEnableChannel(&PWMD2, 0, 256 + v);
		pwmEnableChannel(&PWMD2, 1, 256);
	}
}

void
bot_motor_standby(bool s)
{
	// El pin del TB6612FNG está negado (0 = stby, 1 = correr)
	if (!s)
		palSetPad(GPIOA, GPIOA_MOT_STBY);
	else
		palClearPad(GPIOA, GPIOA_MOT_STBY);
}

int16_t
bot_encoder_izdo(void)
{
	return TIM3->CNT;
}

int16_t
bot_encoder_dcho(void)
{
	return TIM4->CNT;
}


#endif
