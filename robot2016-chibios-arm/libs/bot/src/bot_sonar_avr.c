#include "bot.h"
#if BOT_PLATFORM == 2015

#include <avr/io.h>
#include <avr/interrupt.h>

///////////////////////////////////////////////////////////////////////
// ISRs
///////////////////////////////////////////////////////////////////////

OSAL_IRQ_HANDLER(PCINT0_vect) {
	OSAL_IRQ_PROLOGUE();
	osalSysLockFromISR();

	switch(estado_sonar) {
	case SONAR_OFF:
		break;

	case WAIT_ECHO_CLEAR:
		if (! (PINB & _BV(PINB1)))  // espera 0
			estado_sonar = WAIT_PING_START;
		break;

	case WAIT_PING_START:
		if (PINB & _BV(PINB1)) {   // espera 1 (empieza a contar tiempo)
			estado_sonar = WAIT_PING_END;
			ping_t0 = TCNT1;
		}
		break;

	case WAIT_PING_END:
		if (! (PINB & _BV(PINB1))) {  // espera 0 (fin medición)
			estado_sonar = SONAR_OFF;
			ping_time = TCNT1 - ping_t0;

			/* Wakes up the thread.*/
			chThdResumeI(&trp, ping_time);  /* Resuming the thread with message.*/
		}
		break;
	}

	osalSysUnlockFromISR();
	OSAL_IRQ_EPILOGUE();
}


///////////////////////////////////////////////////////////////////////
// Funciones privadas (static)
///////////////////////////////////////////////////////////////////////

static void
bot_reset_sonar_pin(void) {
	DDRB |= _BV(DDB1);				// Pin to output
	PORTB &= ~_BV(PORTB1);			// Trigger pin to low
}


///////////////////////////////////////////////////////////////////////
// Funciones exportadas
///////////////////////////////////////////////////////////////////////

int bot_ping(void)
{
	enum estado_sonar_t es;
	chSysLock();	// desactiva todas las IRQ pero no las FIQ
	es = estado_sonar;
	chSysUnlock();

	if (es == SONAR_OFF) {
		// This assumes the pin starts as an output, low level (see setup)
		chSysLock();					// CLI
		PORTB |= _BV(PINB1);			// Trigger pin high, tells the sensor to ping
		bot_busy_delay(3);				// Wait 12us for the sensor to ack the trigger
		estado_sonar = WAIT_ECHO_CLEAR;
		PORTB &= ~_BV(PORTB1);			// Trigger pin to low
		DDRB &= ~_BV(DDB1);				// Pin to input

		// Wait for the IRQ to happen. This should happen under chSysLock, which we did before
		int ping_time = chThdSuspendTimeoutS(&trp, 30000);	// 120 ms timeout
		chSysUnlock();

		bot_reset_sonar_pin();			// Output, low

		int mm = ping_time * 4L * 11 / 64;	// 4us/tick
		return mm;
	}
	else {
		estado_sonar = SONAR_OFF;
		bot_reset_sonar_pin();			// Output, low
		return -es;
	}
}



#endif
