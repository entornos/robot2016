#include "bot.h"
#if BOT_PLATFORM == 2016

#include "hal.h"
#include "ch.h"

void
bot_led_placa(bool encender)
{
	if (encender) palClearPad(GPIOC, GPIOC_ONBOARD_LED);
	else palSetPad(GPIOC, GPIOC_ONBOARD_LED);
}

void
bot_led_1(bool encender)
{
	if (encender) palClearPad(GPIOA, GPIOA_RED_LED);
	else palSetPad(GPIOA, GPIOA_RED_LED);
}

void
bot_led_2(bool encender)
{
	if (encender) palClearPad(GPIOB, GPIOB_YELLOW_LED);
	else palSetPad(GPIOB, GPIOB_YELLOW_LED);
}

#endif
