
#include <string.h>
#include <math.h>
#include "bot_mpu.h"
#include "ch.h"
#include "chprintf.h"
#include "hal.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"

///////////////////////////////////////////////////////////////////////
// Macros
///////////////////////////////////////////////////////////////////////

// Esto está en el HAL pero no en NIL
#ifndef ST2MS
#define ST2MS(n) (((n) * 1000UL + NIL_CFG_ST_FREQUENCY - 1UL) /              \
		NIL_CFG_ST_FREQUENCY)
#endif


///////////////////////////////////////////////////////////////////////
// Structs
///////////////////////////////////////////////////////////////////////

struct rx_s {
	unsigned char header[3];
	unsigned char cmd;
};


///////////////////////////////////////////////////////////////////////
// Constantes privadas
///////////////////////////////////////////////////////////////////////

static BaseSequentialStream * const OUT = (BaseSequentialStream *)&SD1;

static int const DEFAULT_MPU_HZ = 100; // Starting sampling rate

/* The sensors can be mounted onto the board in any orientation. The mounting
 * matrix seen below tells the MPL how to rotate the raw data from the
 * driver(s).
 * TODO: The following matrices refer to the configuration on an internal test
 * board at Invensense. If needed, please modify the matrices to match the
 * chip-to-body matrix for your particular set up.
 */
static int8_t const gyro_orientation[9] = {
		1, 0, 0,
		0, 1, 0,
		0, 0, 1
};


///////////////////////////////////////////////////////////////////////
// Variables privadas
///////////////////////////////////////////////////////////////////////

static int16_t gyro[3], accel[3], sensors;
static uint32_t sensor_timestamp;
static int32_t quat[4];		// coma fija q30
static int32_t gravedad[3];		// coma fija q30
static int32_t rumbo[3];		// q30
static int16_t rumbo_gr;		// centésimas de grado


///////////////////////////////////////////////////////////////////////
// Variables exportadas, pero no en el .h
///////////////////////////////////////////////////////////////////////

thread_reference_t mpu_trp = NULL;


///////////////////////////////////////////////////////////////////////
// Funciones estáticas
///////////////////////////////////////////////////////////////////////

/* These next two functions converts the orientation matrix (see
 * gyro_orientation) to a scalar representation for use by the DMP.
 * NOTE: These functions are borrowed from Invensense's MPL.
 */
static inline unsigned short inv_row_2_scale(int8_t const * row)
{
	unsigned short b;

	if (row[0] > 0)
		b = 0;
	else if (row[0] < 0)
		b = 4;
	else if (row[1] > 0)
		b = 1;
	else if (row[1] < 0)
		b = 5;
	else if (row[2] > 0)
		b = 2;
	else if (row[2] < 0)
		b = 6;
	else
		b = 7;      // error
	return b;
}

static inline uint16_t
inv_orientation_matrix_to_scalar(int8_t const * mtx)
{
	unsigned short scalar;

	/*
     XYZ  010_001_000 Identity Matrix
     XZY  001_010_000
     YXZ  010_000_001
     YZX  000_010_001
     ZXY  001_000_010
     ZYX  000_001_010
	 */

	scalar = inv_row_2_scale(mtx);
	scalar |= inv_row_2_scale(mtx + 3) << 3;
	scalar |= inv_row_2_scale(mtx + 6) << 6;

	return scalar;
}

static void
tap_cb(uint8_t direction, uint8_t count)
{
	(void)direction;
	(void)count;
}

static void
android_orient_cb(uint8_t orientation)
{
	(void)orientation;
}


#if 0
static void
self_test(void)
{
	int result;

	/* Self test
	 *
	 */
	long g[3], a[3];
	result = mpu_run_self_test(g, a);
	if (result == 7) {		// LSB->MSB: gyro, accel, compass; 1=pass, 0=fail
		/*
		chprintf((BaseSequentialStream *)&SD1,
				"gt %5ld %5ld %5ld at %5ld %5ld %5ld\r\n",
				g[0], g[1], g[2],
				a[0], a[1], a[2]);
		 */

		// gb 109740, -65235, -151483 ab 3315 -94 -6
		// gb 1799736, -1069854, -2484321 ab 54312960 -1540096 -98304
		// g    27   -18   -39 a   800     0 16360
		chprintf(OUT, "gb %ld, %ld, %ld ab %ld %ld %ld\r\n",
				g[0], g[1], g[2], a[0], a[1], a[2]);
		/* Test passed. We can trust the gyro data here, so let's push it down
		 * to the DMP.
		 */
		float sens;
		unsigned short accel_sens;
		mpu_get_gyro_sens(&sens);
		g[0] *= sens;
		g[1] *= sens;
		g[2] *= sens;
		dmp_set_gyro_bias(g);
		mpu_get_accel_sens(&accel_sens);
		a[0] *= accel_sens;
		a[1] *= accel_sens;
		a[2] *= accel_sens;
		dmp_set_accel_bias(a);
		chprintf(OUT, "gb %ld, %ld, %ld ab %ld %ld %ld\r\n",
				g[0], g[1], g[2], a[0], a[1], a[2]);
	}
}
#endif


static void
calibrate(void)
{
	int i, j;
	int32_t g[3] = {0, 0, 0}, a[3] = {0, 0, 0};

	i = 0;
	do {
		chSysLock();
		int msg = chThdSuspendTimeoutS(&mpu_trp, TIME_INFINITE);
		(void)msg;
		chSysUnlock();

		uint8_t more;
		do {
			i++;
			dmp_read_fifo(gyro, accel, quat, &sensor_timestamp, &sensors, &more);
			for (j = 0; j < 3; j++) {
				g[j] += gyro[j];
				a[j] += accel[j];
			}
		} while (more && i < 64);
	} while (i < 64) ;

	for (j = 0; j < 3; j++) {
		g[j] /= (64 / 2);		// TODO: totally made up factors
		a[j] /= (64 * 8);
	}
	a[2] -= 2048;

	mpu_set_gyro_bias_reg(g);
	mpu_set_accel_bias_6050_reg(a);

	// TODO: acceleration should be hardcoded
	// TODO: do this before DMP init so that the DMP starts out with clean calibrated data
	chprintf(OUT, "gb %ld, %ld, %ld ab %ld %ld %ld\r\n",
			g[0], g[1], g[2], a[0], a[1], a[2]);
}


///////////////////////////////////////////////////////////////////////
// Funciones exportadas
///////////////////////////////////////////////////////////////////////


/* De: https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation
 *
 * Matriz de rotación ortogonal a partir de cuaternión [a, b, c, d]:
 *
 *     [ aa+bb-cc-dd  2bc-2ad      2bd+2ac     ]
 * R = [ 2bc+2ad      aa-bb+cc-dd  2cd-2ab     ]
 *     [ 2bd-2ac      2cd+2ab      aa-bb-cc+dd ]
 *
 * Instrucciones: post-multiplicar R por un vector-columna V para obtener el vector rotado V':
 *
 * V' = VR
 */
static void
calcula_gravedad_y_rumbo(void)
{
	int32_t a = quat[0] >> 15, b = quat[1] >> 15, c = quat[2] >> 15, d = quat[3] >> 15;
	int32_t g[3], r[3];
	//int16_t rgr;
	int i;

	/*            [ 0 ]
	 * Gravedad = [ 0 ] R
	 *            [ 1 ]
	 *
	 *         [ 1 ]
	 * Rumbo = [ 0 ] R
	 *         [ 0 ]
	 *
	 * Reordenado para ayudar al optimizador:
	 */
	r[1] = 2 * (b * c - a * d);
	g[0] = 2 * (b * d - a * c);
	r[2] = 2 * (b * d + a * c);
	g[1] = 2 * (c * d + a * b);
	g[2] = a * a - b * b - c * c + d * d;
	r[0] = a * a + b * b - c * c - d * d;
	//rgr = atan2(heading[1], heading[0]) * 32768 / 3.1415926535897932384626435;


	chSysLock();
	for (i = 0; i < 3; i++) {
		gravedad[i] = g[i];
		rumbo[i] = r[i];
	}
	//rumbo_gr = rgr;
	chSysUnlock();
}


void bot_mpu_init() {

	int result;
	unsigned char accel_fsr;
	unsigned short gyro_rate, gyro_fsr;
	//unsigned long timestamp;


	/* Set up gyro.
	 * Every function preceded by mpu_ is a driver function and can be found
	 * in inv_mpu.h.
	 */
	chprintf(OUT, "mpu_init\r\n");
	do {
		result = mpu_init();

		if (result) {
			chThdSleepMilliseconds(100);
			chprintf(OUT, "oh shit %d\r\n", result);
			chThdSleepMilliseconds(100);
			// TODO: end thread
		}
	} while (result);


	/* If you're not using an MPU9150 AND you're not using DMP features, this
	 * function will place all slaves on the primary bus.
	 * mpu_set_bypass(1);
	 */

	/* Get/set hardware configuration. Start gyro. */
	//mpu_set_accel_fsr(2);	// Set max acceleration to +/-16G. Apparently ignored as soon as we use the DMP -JAVI
	/* Wake up all sensors. */
	mpu_set_sensors(INV_XYZ_GYRO | INV_XYZ_ACCEL);
	/* Push both gyro and accel data into the FIFO. */
	mpu_configure_fifo(INV_XYZ_GYRO | INV_XYZ_ACCEL);
	mpu_set_sample_rate(DEFAULT_MPU_HZ);
	/* Read back configuration in case it was set improperly. */
	mpu_get_sample_rate(&gyro_rate);
	mpu_get_gyro_fsr(&gyro_fsr);
	mpu_get_accel_fsr(&accel_fsr);


	/* To initialize the DMP:
	 * 1. Call dmp_load_motion_driver_firmware(). This pushes the DMP image in
	 *    inv_mpu_dmp_motion_driver.h into the MPU memory.
	 * 2. Push the gyro and accel orientation matrix to the DMP.
	 * 3. Register gesture callbacks. Don't worry, these callbacks won't be
	 *    executed unless the corresponding feature is enabled.
	 * 4. Call dmp_enable_feature(mask) to enable different features.
	 * 5. Call dmp_set_fifo_rate(freq) to select a DMP output rate.
	 * 6. Call any feature-specific control functions.
	 *
	 * To enable the DMP, just call mpu_set_dmp_state(1). This function can
	 * be called repeatedly to enable and disable the DMP at runtime.
	 *
	 * The following is a short summary of the features supported in the DMP
	 * image provided in inv_mpu_dmp_motion_driver.c:
	 * DMP_FEATURE_LP_QUAT: Generate a gyro-only quaternion on the DMP at
	 * 200Hz. Integrating the gyro data at higher rates reduces numerical
	 * errors (compared to integration on the MCU at a lower sampling rate).
	 * DMP_FEATURE_6X_LP_QUAT: Generate a gyro/accel quaternion on the DMP at
	 * 200Hz. Cannot be used in combination with DMP_FEATURE_LP_QUAT.
	 * DMP_FEATURE_TAP: Detect taps along the X, Y, and Z axes.
	 * DMP_FEATURE_ANDROID_ORIENT: Google's screen rotation algorithm. Triggers
	 * an event at the four orientations where the screen should rotate.
	 * DMP_FEATURE_GYRO_CAL: Calibrates the gyro data after eight seconds of
	 * no motion.
	 * DMP_FEATURE_SEND_RAW_ACCEL: Add raw accelerometer data to the FIFO.
	 * DMP_FEATURE_SEND_RAW_GYRO: Add raw gyro data to the FIFO.
	 * DMP_FEATURE_SEND_CAL_GYRO: Add calibrated gyro data to the FIFO. Cannot
	 * be used in combination with DMP_FEATURE_SEND_RAW_GYRO.
	 */
	dmp_load_motion_driver_firmware();
	dmp_set_orientation(
			inv_orientation_matrix_to_scalar(gyro_orientation));
	dmp_register_tap_cb(tap_cb);
	dmp_register_android_orient_cb(android_orient_cb);
	/*
	 * Known Bug -
	 * DMP when enabled will sample sensor data at 200Hz and output to FIFO at the rate
	 * specified in the dmp_set_fifo_rate API. The DMP will then sent an interrupt once
	 * a sample has been put into the FIFO. Therefore if the dmp_set_fifo_rate is at 25Hz
	 * there will be a 25Hz interrupt from the MPU device.
	 *
	 * There is a known issue in which if you do not enable DMP_FEATURE_TAP
	 * then the interrupts will be at 200Hz even if fifo rate
	 * is set at a different rate. To avoid this issue include the DMP_FEATURE_TAP
	 */
	dmp_enable_feature(
			DMP_FEATURE_6X_LP_QUAT |
			DMP_FEATURE_TAP |
			DMP_FEATURE_ANDROID_ORIENT |
			DMP_FEATURE_SEND_RAW_ACCEL |
			DMP_FEATURE_SEND_CAL_GYRO |
			DMP_FEATURE_GYRO_CAL);
	dmp_set_fifo_rate(DEFAULT_MPU_HZ);
	mpu_set_dmp_state(1);


	// Startup calibration
	calibrate();
	//self_test();
}


void bot_mpu_lee_inerciales()
{
	/* Waiting for the IRQ to happen.*/

	chSysLock();
	int msg = chThdSuspendTimeoutS(&mpu_trp, TIME_INFINITE);
	(void)msg;
	chSysUnlock();

	/* This function gets new data from the FIFO when the DMP is in
	 * use. The FIFO can contain any combination of gyro, accel,
	 * quaternion, and gesture data. The sensors parameter tells the
	 * caller which data fields were actually populated with new data.
	 * For example, if sensors == (INV_XYZ_GYRO | INV_WXYZ_QUAT), then
	 * the FIFO isn't being filled with accel data.
	 * The driver parses the gesture data to determine if a gesture
	 * event has occurred; on an event, the application will be notified
	 * via a callback (assuming that a callback function was properly
	 * registered). The more parameter is non-zero if there are
	 * leftover packets in the FIFO.
	 *
	 * Gyro and accel data are written to the FIFO by the DMP in chip
	 * frame and hardware units. This behavior is convenient because it
	 * keeps the gyro and accel outputs of dmp_read_fifo and
	 * mpu_read_fifo consistent.
	 *
	 * Unlike gyro and accel, quaternions are written to the FIFO in
	 * the body frame, q30. The orientation is set by the scalar passed
	 * to dmp_set_orientation during initialization.
	 */
	uint8_t more;
	do {
		dmp_read_fifo(gyro, accel, quat, &sensor_timestamp, &sensors, &more);
		//integraInt();
	} while (more);
	calcula_gravedad_y_rumbo();

#if 0
	chprintf(OUT,
			"t %5ld g %5d %5d %5d a %5d %5d %5d\r\n",
			ST2MS(sensor_timestamp),
			gyro[0], gyro[1], gyro[2],
			accel[0], accel[1], accel[2]);
#endif
#if 0
	chprintf(OUT, "gr %4d %4d %4d\r\n",
			(int16_t)(gravity[0] / 1073742L),
			(int16_t)(gravity[1] / 1073742L),
			(int16_t)(gravity[2] / 1073742L));
#endif
#if 0
	chprintf(OUT, "he %4d %4d %4d ru %d\r\n",
			(int16_t)(heading[0] / 1073742L),
			(int16_t)(heading[1] / 1073742L),
			(int16_t)(heading[2] / 1073742L),
			rumbo_gr);
#endif
#if 0
	chprintf(OUT, "q %5ld, %5ld, %5ld, %5ld\r\n",
			quat[0], quat[1], quat[2], quat[3]);
#endif
}

int32_t bot_gra_x(void)
{
	chSysLock();
	int32_t r = gravedad[0];
	chSysUnlock();
	return r;
}

int32_t bot_gra_y(void)
{
	chSysLock();
	int32_t r = gravedad[1];
	chSysUnlock();
	return r;
}

int32_t bot_gra_z(void)
{
	chSysLock();
	int32_t r = gravedad[2];
	chSysUnlock();
	return r;
}

int32_t bot_rum_x(void)
{
	chSysLock();
	int32_t r = rumbo[0];
	chSysUnlock();
	return r;
}

int32_t bot_rum_y(void)
{
	chSysLock();
	int32_t r = rumbo[1];
	chSysUnlock();
	return r;
}

int32_t bot_rum_z(void)
{
	chSysLock();
	int32_t r = rumbo[2];
	chSysUnlock();
	return r;
}

int16_t bot_rum_gr(void)
{
	chSysLock();
	int16_t r = rumbo_gr;
	chSysUnlock();
	return r;
}
