#include "bot.h"
#if BOT_PLATFORM == 2015

///////////////////////////////////////////////////////////////////////
// ISRs
///////////////////////////////////////////////////////////////////////

OSAL_IRQ_HANDLER(ADC_vect) {

#if 0
	OSAL_IRQ_PROLOGUE();
	/* Wakes up the thread.*/
	osalSysLockFromISR();	// lo mismo que chXXX? TODO
	chThdResumeI(&trp, (msg_t)0x1337);  /* Resuming the thread with message.*/
	osalSysUnlockFromISR();
	OSAL_IRQ_EPILOGUE();
#endif
}


///////////////////////////////////////////////////////////////////////
// Funciones estáticas
///////////////////////////////////////////////////////////////////////

static uint8_t
bot_lee_adc(int canal) {
	// Usa como referencia AVcc (REFS0), alinea a la izda porque solo vamos a
	// usar los 8 bits mas altos (ADLAR) y selecciona el canal (p.266)
	ADMUX = _BV(REFS0) | _BV(ADLAR) | canal;

	// Inicia la conversion
	ADCSRA |= _BV(ADSC) | _BV(ADEN);

	// Espera a que acabe (TODO: espera a la IRQ?)
	do {/*
		chSysLock();
		int msg = chThdSuspendTimeoutS(&trp, TIME_INFINITE);
		(void)msg;
		chSysUnlock();
	 */
	} while (ADCSRA & _BV(ADSC)) ;

	// Devuelve la muestra
	return ADCH;
}


///////////////////////////////////////////////////////////////////////
// Funciones exportadas
///////////////////////////////////////////////////////////////////////

void
bot_lee_cny70(uint8_t s[])
{
	s[0] = bot_lee_adc(0);
	s[1] = bot_lee_adc(1);
	s[2] = bot_lee_adc(2);
	s[3] = bot_lee_adc(3);
	s[4] = bot_lee_adc(6);
	s[5] = bot_lee_adc(7);
}


#endif
