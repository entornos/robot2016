#include "bot.h"
#if BOT_PLATFORM == 2015

#include <avr/io.h>
#include <avr/interrupt.h>
#include "nil.h"
#include "hal.h"
#include "chprintf.h"

///////////////////////////////////////////////////////////////////////
// Constantes privadas
///////////////////////////////////////////////////////////////////////

static BaseSequentialStream * const OUT = (BaseSequentialStream *)&SD1;


///////////////////////////////////////////////////////////////////////
// Variables privadas
///////////////////////////////////////////////////////////////////////

static volatile uint16_t t1_ovf_count = 0;


///////////////////////////////////////////////////////////////////////
// ISRs
///////////////////////////////////////////////////////////////////////

OSAL_IRQ_HANDLER(TIMER1_OVF_vect) {
	t1_ovf_count++;
}


///////////////////////////////////////////////////////////////////////
// Funciones exportadas
///////////////////////////////////////////////////////////////////////

uint32_t
bot_micros(void)
{
	// TODO: asume 4us por tick del timer 1 (por el PRESCALER de st_lld.c)
	uint8_t oldSREG = SREG;
	cli();
	uint32_t m = t1_ovf_count;
	uint16_t t = TCNT1;
	uint8_t o = TIFR1 & _BV(TOV1);

	if (o && t < 0x8000) m++;

	SREG = oldSREG;

	return (((uint32_t)m << 16) + t) * 4;
}

#if 0
uint32_t
bot_micros_test(uint16_t tcnt1)
{
	// Cambiar el PRESCALER de st_lld.c a clkIO/1 para probar esto
	uint8_t oldSREG = SREG;
	cli();

	TCNT1 = 0;
	TIFR1 |= _BV(TOV1);		// writing an 1 clears TOV1
	t1_ovf_count = 0;
	TCNT1 = tcnt1;

	uint32_t m = t1_ovf_count;
	uint16_t t = TCNT1;
	uint8_t o = TIFR1 & _BV(TOV1);

	if (o && t < 0x8000) m++;

	SREG = oldSREG;
	uint32_t r = (((uint32_t)m << 16) + t) * 4;

	chprintf(OUT, "tcnt1=%u m=%lu t=%u o=%u -> %lu\r\n", tcnt1, m, t, o, r);

	return r;
}
#endif

#endif
