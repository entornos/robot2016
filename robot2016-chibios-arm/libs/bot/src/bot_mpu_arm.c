#include "bot.h"
#if BOT_PLATFORM == 2016

#include "ch.h"
#include "hal.h"

///////////////////////////////////////////////////////////////////////
// Variables externas
///////////////////////////////////////////////////////////////////////

extern thread_reference_t mpu_trp;

///////////////////////////////////////////////////////////////////////
// ISRs (exportadas)
///////////////////////////////////////////////////////////////////////

// The MPU6050 interrupt is a short LOW pulse, then it goes back to HIGH
// for the rest of the period. We get interrupts at DEFAULT_MPU_HZ, eg.
// 100 Hz regardless of the fifo being full. Also, we start getting them
// as soon as mpu_configure_fifo is called.
void
mpu6050_isr(EXTDriver *extp, expchannel_t channel)
{
	(void)extp;		// suppress gcc warnings
	(void)channel;

	//OSAL_IRQ_PROLOGUE();	// overflows the stack, you IDIOT

	/* Wakes up the thread.*/
	osalSysLockFromISR();	// same as chXXX
	chThdResumeI(&mpu_trp, (msg_t)0x1337);  /* Resuming the thread with message.*/
	osalSysUnlockFromISR();

	//OSAL_IRQ_EPILOGUE();  // overflows the stack, you IDIOT
}

#endif
