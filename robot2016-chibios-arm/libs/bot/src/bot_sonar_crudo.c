#include "bot.h"
#if BOT_PLATFORM == 2016 && defined(BOT_SONAR_CRUDO)

//#include <math.h>
#include "ch.h"
#include "hal.h"
#include "chprintf.h"

///////////////////////////////////////////////////////////////////////
// Constantes configurables
///////////////////////////////////////////////////////////////////////

#define ADC_FULL_BUF_DEPTH 	32		// tamaño del doble buffer
#define ADC_HALF_BUF_DEPTH 	(ADC_FULL_BUF_DEPTH / 2)	// numero de muestras por ventana
#define ADC_CH_NUM 			1		// canales del ADC a muestrear
#define MAX_DISTANCE 		256		// distancia máxima a muestrear (en ventanas)
#define RMS_SQ_THRESHOLD	200		// cuadrado del valor eficaz umbral (por encima se considera obstáculo)

/*
 * Según ADC_SMPR2 la conversión tardará 12.5 ciclos + ... (RM0008 11.6)
 *
 * 000: 1.5 cycles
 * 001: 7.5 cycles
 * 010: 13.5 cycles
 * 011: 28.5 cycles
 * 100: 41.5 cycles
 * 101: 55.5 cycles
 * 110: 71.5 cycles
 * 111: 239.5 cycles
 */


#define ADC_SMPR2 			02000000000		// Ver RM0008, registros del ADC
#define ADC_CONV_CYCLES 	13.5
#define ADC_CLOCK_FREQ 		12000000		// Hz
#define T_NYQUIST			(0.5 / 40000)	// Período de nyquist (seg)
// Tiempo en muestrear una ventana
#define T_WINDOW			(ADC_HALF_BUF_DEPTH * (12.5 + ADC_CONV_CYCLES) / ADC_CLOCK_FREQ)
// Tiempo en muestrear la distancia total
#define T_PING 				(MAX_DISTANCE * T_WINDOW)	// s por medición

/*
 * La transmisión tarda 248 us (carga del max232) + 200 us (8 pulsos a 40 KHz). Tomamos
 * como inicio de la tranmisión cuando llevamos 4 pulsos enviados.
 */
#define T_TX_RAMP_UP		((248.0 + 200 / 2) / 1000000)			// seg en ir por la mitad de tx
#define RAMP_UP_DISTANCE	((int32_t)(T_TX_RAMP_UP / T_WINDOW))	// ventanas en ir por la mitad de tx

/*
 * 340000/2 mm     --- 1 seg
 * MAX_DISTANCE_MM --- T_PING seg
 */
#define MAX_DISTANCE_MM		((int32_t)(T_PING * 340000 / 2 + 0.5))	// Util para convertir luego

/*
 * Compensación de energía vs distancia (usando pow() -> MUY LENTO)
 */
#define COMP_B				1902756.19759586
#define COMP_M				0.9958912937

/*
 * Compensación de energía vs distancia (optimizado)
 */
#define INV_RATIO_Y1_Y0_Q16	67146	// constantes arcanas, ver sonar.ods
#define INV_YD0_X1000_Q24	8817


///////////////////////////////////////////////////////////////////////
// Constantes estáticas
///////////////////////////////////////////////////////////////////////

static BaseSequentialStream * const OUT = (BaseSequentialStream *)&SD1;


///////////////////////////////////////////////////////////////////////
// Variables privadas
///////////////////////////////////////////////////////////////////////

static volatile int distance;			// distancia en ventanas
static volatile int ramp_up_distance;	// ventanas a esperar mientras se emite
static int32_t rms_buf[MAX_DISTANCE];
static adcsample_t samples_buf[ADC_FULL_BUF_DEPTH * ADC_CH_NUM];		// results array
static int32_t y = INV_YD0_X1000_Q24;	// y = función normalizadora de la energía del eco

static const GPTConfig gptCfg = {
	1000000,	// Freq in Hz (100 000), max count time = 65536 / 100000 = 655.36 ms
	NULL,
	0,		// cr2
	0		// dier
};

static void adc_callback(ADCDriver *adcp, adcsample_t *buffer, size_t n);

static ADCConversionGroup adccg = {
	true,			// circular buffer, false otherwise
	1,				// number of channels
	adc_callback,	// callback function
	NULL,			// error callback
	0,				// CR1
	0, 				// CR2
	0,				// SMPR1
	ADC_SMPR2,		// SMPR2 (octal since they are groups of 3 bits)
	// SQR1: channel sequence length (num channels - 1) << 20
	(ADC_CH_NUM - 1) << 20,
	// SQR2: no more channels in sequence
	0,
	// SQR3: channel 9
	9
};


///////////////////////////////////////////////////////////////////////
// Variables exportadas
///////////////////////////////////////////////////////////////////////

mutex_t bot_sonar_mutex;


///////////////////////////////////////////////////////////////////////
// Vestigios de la librería vieja
///////////////////////////////////////////////////////////////////////

void
sonar_isr(EXTDriver *extp, expchannel_t channel)
{
	(void)extp;		// suppress gcc warnings
	(void)channel;
}


///////////////////////////////////////////////////////////////////////
// Functiones estáticas
///////////////////////////////////////////////////////////////////////


static inline int32_t
us_to_ticks(int32_t us)
{
	return us * STM32_HCLK / 1000000;
}

static inline void
delay_ticks(int32_t ticks) {
	int32_t t1 = SysTick->VAL, t0;
	int32_t total_elapsed = 0;
	do {
		t0 = t1;
		t1 = SysTick->VAL;
		int32_t elapsed = t0 - t1;	// t0-t1 because SysTick counts backwards
		if (elapsed < 0) elapsed += (STM32_HCLK / OSAL_ST_FREQUENCY);	// cheap modulo
		total_elapsed += elapsed;	// bigger total so that we can wait longer than 72000 us
	} while (total_elapsed < ticks);
}


static void
adc_callback(ADCDriver *adcp, adcsample_t *buffer, size_t n)
{
	(void)adcp;
	(void)n;

	int32_t rms_sq = 0;
	int32_t avg = 0;
	size_t j;

	// Espera a que empiece a emitir
	if (ramp_up_distance <= RAMP_UP_DISTANCE) {
		ramp_up_distance++;
		return;
	}

	// Se supone que nos van pasando la mitad alta y la mitad
	// baja del buffer, alternadamente, y que n siempre vale
	// ADC_HALF_BUF_DEPTH.

	// Toma la media.
	for (j = 0; j < ADC_HALF_BUF_DEPTH; j++) {
		avg += buffer[j];
	}
	avg /= ADC_HALF_BUF_DEPTH;

	// Toma el cuadrado del valor eficaz
	for (j = 0; j < ADC_HALF_BUF_DEPTH; j++) {
		int32_t s = samples_buf[j] - avg;
		rms_sq += s * s;
	}

	// Compensa por distancia
	rms_sq /= ADC_HALF_BUF_DEPTH;
	rms_sq = ((int64_t)y * rms_sq) >> 24;
	y = ((int64_t)y * INV_RATIO_Y1_Y0_Q16) >> 16;

	// Guarda
	rms_buf[distance] = rms_sq;

	distance++;
	if (distance >= MAX_DISTANCE) {
		chSysLockFromISR();
		distance = 0;
		ramp_up_distance = 0;
		y = INV_YD0_X1000_Q24;
		adcStopConversionI(adcp);		// Solo se puede llamar en región crítica
		chSysUnlockFromISR();
	}
}

/**
 * Sube el cursor n líneas
 */
static void
go_up(int n) {
	//chprintf(OUT, "%c[%d;%df", 0x1B, y, x);	// gotoxy
	chprintf(OUT, "%c[%dA", 0x1b, n);
}


static int
distancia_al_primer_obstaculo(void)
{
	int i;
	for (i = 0; i < MAX_DISTANCE; i++) {
		if (rms_buf[i] > RMS_SQ_THRESHOLD) {
			return i * MAX_DISTANCE_MM / MAX_DISTANCE;
		}
	}
	return MAX_DISTANCE_MM;
}


///////////////////////////////////////////////////////////////////////
// Functiones exportadas
///////////////////////////////////////////////////////////////////////


void
bot_datos_sonar(void)
{
	int i;
	chprintf(OUT, "rms sq");
	for (i = 0; i < MAX_DISTANCE; i++) {
		chprintf(OUT, " %d", rms_buf[i]);
	}
	chprintf(OUT, "\r\n");
}

void
bot_grafica_sonar(int mm)
{
	int i;
	chprintf(OUT, "Sonar rud=%d d=%d (max %d)\r\n", RAMP_UP_DISTANCE, mm, MAX_DISTANCE_MM);

	for (i = 10; i > 0; i--) {
		int j;
		for (j = 0; j < MAX_DISTANCE; j++) {
			if (rms_buf[j] > i * 100) {
				chprintf(OUT, "*");
			} else {
				chprintf(OUT, " ");
			}
		}
		chprintf(OUT, "\r\n");
	}
	for (i = 0; i < MAX_DISTANCE; i++)
		chprintf(OUT, "-");
	chprintf(OUT, " mm\r\n");

	i = 1000;
	do {
		int d;
		for (d = 0; d < MAX_DISTANCE; d++) {
			int d_mm = d * MAX_DISTANCE_MM / MAX_DISTANCE;
			chprintf(OUT, "%d", (d_mm / i) % 10);
		}
		i /= 10;
		chprintf(OUT, "\r\n");
	} while (i >= 1);

	go_up(16);
}


int
bot_sonar_ping(void)
{
	//gptStart(&GPTD1, &gptCfg);


	chSysLock();

	// Wait for pin to go low, testing show 4us is ok
	//gptPolledDelay(&GPTD1, 2);

	// Trigger pin high, tells the sensor to ping
	palSetPad(GPIOB, GPIOB_SONAR_TRIG);
	delay_ticks(us_to_ticks(10));	// TODO: A BOLEO
	// gptPolledDelay(&GPTD1, 20000); TODO: ESTO NO VA!!!!!!!!!
	palClearPad(GPIOB, GPIOB_SONAR_TRIG);

	chSysUnlock();

	/*
	 * 340/2 m (i/v) --- 1 s  (el eco de un obstáculo a 340/2 m tarda 1 seg en volver
	 * 1 m           --- x s  (como mucho queremos detectar obstáculos a 1 m)
	 *
	 * x = 1/170 = 5,88 ms
	 *
	 * Cada ventana de 1 cm durará 5,88 ms / 100 = Tv = 58,8 us
	 *
	 * Muestreando a nyquist (2 * 40 KHz), Ts = 1/(80E3) = 12,5 us.
	 *
	 * En cada ventana habrá Tv/Ts = Tv * Fs = 4,704 muestras.
	 *
	 * Para tener 16 muestras tendríamos que muestrear a Fs = 16 / Tv = 272109 Hz -> Ts = 3,67 us
	 *
	 * Vamos a usar un doble buffer para leer las 16 muestras en modo contínuo del ADC,
	 * y mientras se lee un buffer calculamos el valor eficaz del otro.
	 *
	 * Con el ADC configurado a 12 MHz (mcuconf.h), con ADC_SMPRx:
	 *
 	 * 111 (239.5 + 12.5 ciclos), Tconv = 252/12Mhz = 21 us
	 * 110 (71,5 + 12,5) Tconv = 7 us
	 * 100 (41.5 + 12.5) Tconv = 4.5 us
	 * 011 (28.5 + 12.5) Tconv = 3.4 us <-- ESTE
	 *
	 */

	// int32_t t0 = bot_micros();
	adcAcquireBus(&ADCD1);
	adcConvert(&ADCD1, &adccg, samples_buf, ADC_FULL_BUF_DEPTH);
	adcReleaseBus(&ADCD1);
	// int32_t t1 = bot_micros();

	// Compensa el valor eficaz de la intensidad del sonido con la inversa de la distancia
#if 0
	int i;
	for (i = 0; i < MAX_DISTANCE; i++) {
		//rms_buf[i] = rms_buf[i] * COMP_B * expf(COMP_M * i * MAX_DISTANCE_MM / MAX_DISTANCE);
		rms_buf[i] = 1000 * rms_buf[i] / (COMP_B * pow(COMP_M, i * MAX_DISTANCE_MM / MAX_DISTANCE));
	}
#endif
	int i;
	y = INV_YD0_X1000_Q24;
	for (i = 0; i < MAX_DISTANCE; i++) {
	}

	/*
	 * Aquí podríamos poner la pata ECHO en modo salida y meterle un 0 para
	 * forzar al MCU del HC-SR04 a salir del coma en caso de que no haya eco, pero
	 * no hace falta porque al meterle una resistencia de 4K7 a GND para acondicionar
	 * la salida del último OpAmp a [0, 3.3V], el valor DC sin ecos ya está por debajo
	 * del umbral (que es cuando se genera un 1 en la pata "signal").
	 */

	return distancia_al_primer_obstaculo();
}

int32_t
bot_sonar_valor_eficaz_a(int32_t mm)
{
	int32_t d = mm * MAX_DISTANCE / MAX_DISTANCE_MM;
	if (d < 0 || d >= MAX_DISTANCE)
		return -1;
	return rms_buf[d];
}

#endif
