#include "bot.h"
#if BOT_PLATFORM == 2015

#include <avr/io.h>

bool bot_lata_negra(void);
{
	return (PIND & _BV(PIND4)) ? 1 : 0;
}

#endif
