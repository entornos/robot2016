#include "bot.h"
#if BOT_PLATFORM == 2016

#include "ch.h"
#include "hal.h"
#include "chprintf.h"

///////////////////////////////////////////////////////////////////////
// Constantes privadas
///////////////////////////////////////////////////////////////////////

static BaseSequentialStream * const OUT = (BaseSequentialStream *)&SD1;


///////////////////////////////////////////////////////////////////////
// Variables exportadas
///////////////////////////////////////////////////////////////////////

volatile uint32_t time_ovf = 0;


///////////////////////////////////////////////////////////////////////
// Funciones exportadas
///////////////////////////////////////////////////////////////////////

uint32_t
bot_micros(void)
{
	__disable_irq();	// asm("cpsid i");

	uint32_t m = time_ovf;
	uint32_t t = SysTick->VAL;		// TODO: assume HCLK = 72 MHz

	// Check pending overflow IRQ after we disabled interrupts
	uint32_t o = SCB->ICSR & SCB_ICSR_PENDSTSET_Msk;

	// If overflow and counter rolled over, add 1000 to the microseconds count
	if (o && t > 36000) m+= 1000;

	__enable_irq();		// asm("cpsie i");

	// Systick counts downwards, so subtract it from 999.
	return (m + (999 - t / 72));
}

#if 0
uint32_t
bot_micros_test(uint16_t tcnt1)
{
	// Cambiar el PRESCALER de st_lld.c a clkIO/1 para probar esto
	uint8_t oldSREG = SREG;
	cli();

	TCNT1 = 0;
	TIFR1 |= _BV(TOV1);		// writing an 1 clears TOV1
	t1_ovf_count = 0;
	TCNT1 = tcnt1;

	uint32_t m = t1_ovf_count;
	uint16_t t = TCNT1;
	uint8_t o = TIFR1 & _BV(TOV1);

	if (o && t < 0x8000) m++;

	SREG = oldSREG;
	uint32_t r = (((uint32_t)m << 16) + t) * 4;

	chprintf(OUT, "tcnt1=%u m=%lu t=%u o=%u -> %lu\r\n", tcnt1, m, t, o, r);

	return r;
}
#endif

#endif
