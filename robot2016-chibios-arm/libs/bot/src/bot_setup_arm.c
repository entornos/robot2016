#include "bot.h"
#if BOT_PLATFORM == 2016

#include "ch.h"
#include "hal.h"


static const SerialConfig uartCfg = {
		230400,
		0,
		0,
		0
};

static const I2CConfig i2ccfg = {
		OPMODE_I2C,
		100000, // 400000, // 100000
		STD_DUTY_CYCLE, // FAST_DUTY_CYCLE_2, // STD_DUTY_CYCLE,
};

extern void mpu6050_isr(EXTDriver *extp, expchannel_t channel);
extern void sonar_isr(EXTDriver *extp, expchannel_t channel);

static const EXTConfig extcfg = {
		{
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_DISABLED, NULL},
				{EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOB, mpu6050_isr},
				{EXT_CH_MODE_BOTH_EDGES | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOB, sonar_isr}
		}
};

static const PWMConfig pwmCfg = {
   288000,	// PWM clock (Hz) <-- change this to change freq
   256,		// Counts each PWM cycle; PWM freq = 288000 / 256 = 1125 Hz
   NULL,	// Callback
   {
      {PWM_OUTPUT_ACTIVE_HIGH, NULL},
      {PWM_OUTPUT_ACTIVE_HIGH, NULL},
      {PWM_OUTPUT_ACTIVE_HIGH, NULL},
      {PWM_OUTPUT_ACTIVE_HIGH, NULL}
   },
   0,		// TIM CR2 register initialization data
   0		// TIM DIER register initialization data
};

static const GPTConfig gptCfg = {
	100000,	// Freq in Hz (100 000), max count time = 65536 / 100000 = 655.36 ms
	NULL,
	0,		// cr2
	0		// dier
};

// ADCCofnig for stm32 MCUs is empty
static const ADCConfig adcCfg = { 0 };


void
bot_setup(void)
{
	// Remapeo de pines (RM p.185, datasheet p.30):
	// - I2C
	// - PWM TIM2/CH1-4 (full remap)
	// - SWJ config[2:0] = 010 (RM p.177) = disable JTAG, enable SWD
	// - TIM3/CH1-2 (partial remap: PB4, PB5), RM p.178
	AFIO->MAPR = (AFIO_MAPR_I2C1_REMAP
			| AFIO_MAPR_TIM2_REMAP_FULLREMAP
			| AFIO_MAPR_SWJ_CFG_1
			| AFIO_MAPR_TIM3_REMAP_1);

	// Init drivers
	sdStart(&SD1, &uartCfg);
	i2cStart(&I2CD1, &i2ccfg);
	extStart(&EXTD1, &extcfg);
	pwmStart(&PWMD2, &pwmCfg);
	gptStart(&GPTD1, &gptCfg);
	adcStart(&ADCD1, &adcCfg);

	// Hand the pins to the I2C controller
	palSetPadMode(GPIOB, GPIOB_I2C_SCL, PAL_MODE_STM32_ALTERNATE_OPENDRAIN);
	palSetPadMode(GPIOB, GPIOB_I2C_SDA, PAL_MODE_STM32_ALTERNATE_OPENDRAIN);

	///////////////////////////////////////////////////////////////////
	// QEI
	///////////////////////////////////////////////////////////////////

	// Enable TIM3, 4
	RCC->APB1ENR |= (RCC_APB1ENR_TIM3EN | RCC_APB1ENR_TIM4EN);

	// RM 15.3.12 - modo encoder, cuenta los 2 flancos
	TIM3->SMCR = TIM4->SMCR = (TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1);

	// AN4013 p.31: http://www.st.com/web/en/resource/technical/document/application_note/DM00042534.pdf
	// TI1 connected to TI1FP1 CC1S='01' in CCMR1 register
	// TI2 connected to TI2FP2 CC2S='01' in CCMR1 register
	TIM3->CCMR1 |= (TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_0);
	TIM4->CCMR1 |= (TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_0);

	// Reverse polarity of TIM3 (count backwards)
	TIM3->CCER |= TIM_CCER_CC1P;

	// Auto reload register (por defecto cuenta de 0 a 0xffff)
	//TIM3->ARR = TIM4->ARR = 0xFFFF;

	// Counter enable
	TIM3->CR1 |= TIM_CR1_CEN;
	TIM4->CR1 |= TIM_CR1_CEN;

	// Empieza frenado
	bot_velocidad(0, 0);

	// Inicia semáforos
	chMtxObjectInit(&bot_sonar_mutex);
}


#endif
