#include "bot.h"
#if BOT_PLATFORM == 2016

#include "hal.h"
#include "ch.h"

#define ADC_BUF_DEPTH 1
#define ADC_CH_NUM 6

static adcsample_t samples_buf[ADC_BUF_DEPTH * ADC_CH_NUM];		// results array

static ADCConversionGroup adccg = {
	false,			// circular buffer, false otherwise
	ADC_CH_NUM,		// number of channels
	NULL,			// callback function
	NULL,			// error callback
	0,				// CR1
	0, 				// CR2
	0,				// SMPR1
	// TODO: en SMPR2 van los ciclos de muestreo (RM0008 11.12.6),
	// hay que afinar esto y poner el mínimo tiempo que haga que el negro se vea como 255.
	04444440,		// SMPR2 (octal since they are groups of 3 bits)
	// SQR1: channel sequence length
	(ADC_CH_NUM - 1) << 20,
	// SQR2: no more channels in sequence
	0,
	// SQR3: channels 1 to 6 (PA1-6)
	1 | (2 << 5) | (3 << 10) | (4 << 15) | (5 << 20) | (6 << 25)
};


void
bot_lee_cny70(uint8_t s[])
{
	adcAcquireBus(&ADCD1);
	adcConvert(&ADCD1, &adccg, samples_buf, ADC_BUF_DEPTH);
	adcReleaseBus(&ADCD1);

	// El ADC del STM32F1 tiene 12 bits, pasa a 8 para ser compatible con el del AVR
	s[0] = samples_buf[0] >> 4;
	s[1] = samples_buf[1] >> 4;
	s[2] = samples_buf[2] >> 4;
	s[3] = samples_buf[3] >> 4;
	s[4] = samples_buf[4] >> 4;
	s[5] = samples_buf[5] >> 4;
}

void bot_enciende_cny70(bool encendido)
{
	if (encendido) {
		palSetPad(GPIOB, GPIOB_LEDON);
	} else {
		palClearPad(GPIOB, GPIOB_LEDON);
	}
}


#endif
