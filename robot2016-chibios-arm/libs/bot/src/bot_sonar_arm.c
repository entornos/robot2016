#include "bot.h"
#if BOT_PLATFORM == 2016 && !defined(BOT_SONAR_CRUDO)

#include "ch.h"
#include "hal.h"

// Elegir el tipo de sónar:

// HC-SR04 de banggood (2016), código: 942912 etiquetado "SKU339956-F990 PCK", lleva marcados
// una T y una R en los transductores.
// URL: http://www.banggood.com/10Pcs-HC-SR04-Ultrasonic-Ranging-Sensor-Ultrasonic-Module-For-Arduino-p-942912.html
// Estos HC-SR04 no tienen timeout cuando pierden el eco. Hay que sacarlos... COMO???
// No llevan pull-up en TRIG.
#define HC_SR04_NON_TIMING_OUT

// HC-SR04 ???
// URL: ???
// Este HC-SR04 tiene un timeout de 190 ms, se puede medir fácilmente tapando uno de los transductores.
// A veces cuando da timeout, la siguiente lectura es basura (da siempre 40-50 mm).
// #define HC_SR04_190



///////////////////////////////////////////////////////////////////////
// Enums
///////////////////////////////////////////////////////////////////////

enum estado_sonar_t {
		SONAR_OFF,
		WAIT_ECHO_CLEAR,
		WAIT_PING_START,
		WAIT_PING_END
};

///////////////////////////////////////////////////////////////////////
// Variables estáticas
///////////////////////////////////////////////////////////////////////

static enum estado_sonar_t estado_sonar;
static thread_reference_t sonar_trp = NULL;
static int ping_t0;


///////////////////////////////////////////////////////////////////////
// ISRs (exportadas)
///////////////////////////////////////////////////////////////////////

void
sonar_isr(EXTDriver *extp, expchannel_t channel)
{
	(void)extp;		// suppress gcc warnings
	(void)channel;

	chSysLockFromISR();

	switch(estado_sonar) {
	case SONAR_OFF:
		break;
	case WAIT_ECHO_CLEAR:
		if (! palReadPad(GPIOB, 15))  // espera 0
			estado_sonar = WAIT_PING_START;
		break;

	case WAIT_PING_START:
		if (palReadPad(GPIOB, 15)) {   // espera 1 (empieza a contar tiempo)
			estado_sonar = WAIT_PING_END;
			ping_t0 = gptGetCounterX(&GPTD1);
		}
		break;

	case WAIT_PING_END:
		if (! palReadPad(GPIOB, 15)) {  // espera 0 (fin medición)
			estado_sonar = SONAR_OFF;
			int ping_time = gptGetCounterX(&GPTD1) - ping_t0;

			/* Wakes up the thread.*/
			chThdResumeI(&sonar_trp, ping_time);  /* Resuming the thread with message.*/
		}
		break;
	}

	chSysUnlockFromISR();
}


///////////////////////////////////////////////////////////////////////
// Functiones estáticas
///////////////////////////////////////////////////////////////////////

static inline void delay_until_us(int32_t start, int32_t us) {
	int32_t elapsed;
	int32_t ticks = us * (STM32_HCLK / 1000000);
	//chSysLock();
	do {
		elapsed = start - SysTick->VAL;
		if (elapsed < 0) elapsed += (STM32_HCLK / OSAL_ST_FREQUENCY);
	} while (elapsed < ticks);
	//chSysUnlock();
}


///////////////////////////////////////////////////////////////////////
// Functiones exportadas
///////////////////////////////////////////////////////////////////////

int
bot_sonar_ping(void)
{
	int es, ping_time;
	chSysLock();	// desactiva todas las IRQ pero no las FIQ
	es = estado_sonar;
	chSysUnlock();

	if (es == SONAR_OFF) {
		// Pin to out, low
		palSetPadMode(GPIOB, GPIOB_TRIGECHO, PAL_MODE_OUTPUT_PUSHPULL);
		palClearPad(GPIOB, GPIOB_TRIGECHO);

		// CLI
		chSysLock();

		// Wait for pin to go low, testing show 4us is ok
		gptPolledDelay(&GPTD1, 2);

		// Trigger pin high, tells the sensor to ping
		palSetPad(GPIOB, GPIOB_TRIGECHO);

		// Wait 10us for the sensor to ack the trigger
		gptPolledDelay(&GPTD1, 2);
		estado_sonar = WAIT_ECHO_CLEAR;

		// Trigger pin low (very important if this is the non-timing out sonar so that it forces a new ping)
		// We have ~450 us between the end of the trigger pulse and the beginning of the echo pulse.
		palClearPad(GPIOB, GPIOB_TRIGECHO);
		gptPolledDelay(&GPTD1, 2);

		// Pin to input
		palSetPadMode(GPIOB, GPIOB_TRIGECHO, PAL_MODE_INPUT_PULLDOWN);

		// Start timer 1. I-class call because it croaks with SV#4 otherwise (double lock?)
		gptStartContinuousI(&GPTD1, 65536);


#if defined(HC_SR04_NON_TIMING_OUT)
		// Wait for IRQ (while in chSysLock), 200 ms at most
		msg_t r = chThdSuspendTimeoutS(&sonar_trp, MS2ST(100));

		// SEI
		chSysUnlock();

		// Stop the timer
		gptStopTimer(&GPTD1);

		if (r == MSG_TIMEOUT) {
			estado_sonar = SONAR_OFF;
			ping_time = gptGetCounterX(&GPTD1) - ping_t0;

			// Wait 200 more ms
			chThdSleepMilliseconds(200);

			// Set to 0 for 1ms
			palSetPadMode(GPIOB, GPIOB_TRIGECHO, PAL_MODE_OUTPUT_PUSHPULL);
			palClearPad(GPIOB, GPIOB_TRIGECHO);

			// Set to input for 1ms to reset the sonar.
			//palSetPadMode(GPIOB, GPIOB_TRIGECHO, PAL_MODE_INPUT);
			//gptPolledDelay(&GPTD1, 12000);	// 1,5 ms
			//chThdSleepMilliseconds(1);

		} else {
			ping_time = r;
		}

		// Pull down to 0
		palSetPadMode(GPIOB, GPIOB_TRIGECHO, PAL_MODE_OUTPUT_PUSHPULL);
		palClearPad(GPIOB, GPIOB_TRIGECHO);

#endif

#if defined(HC_SR04_190)
		// This HC-SR04 is guaranteed to timeout when there's no echo
		ping_time = chThdSuspendTimeoutS(&sonar_trp, TIME_INFINITE);

		// SEI
		chSysUnlock();

		// Stop the timer
		gptStopTimer(&GPTD1);

#endif


		// Si timer 1 está configurado con F=100 KHz, cada tick = 100000/72 uS
		int mm = ping_time * 110 / 64;		// ticks del timer 1 -> mm
		return mm;
	}
	else {
		return 0;
	}
}


#endif

