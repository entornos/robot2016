#include "bot.h"
#if BOT_PLATFORM == 2016

#include "ch.h"
#include "hal.h"

#define ADC_BUF_DEPTH 1
#define ADC_CH_NUM 2

static adcsample_t samples_buf[ADC_BUF_DEPTH * ADC_CH_NUM];		// results array

static ADCConversionGroup adccg = {
	false,			// circular buffer, false otherwise
	ADC_CH_NUM,		// number of channels
	NULL,			// callback function
	NULL,			// error callback
	0,				// CR1
	0, 				// CR2
	0,				// SMRP1
	0,				// SMRP2
	// SQR1: channel sequence length
	(ADC_CH_NUM - 1) << 20,
	// SQR2: no more channels in sequence
	0,
	// SQR3: channels 7 to 8 (PA7, PB0)
	7 | (8 << 5),
};


bool
bot_lata_negra(void)
{
	adcAcquireBus(&ADCD1);
	adcConvert(&ADCD1, &adccg, samples_buf, ADC_BUF_DEPTH);
	adcReleaseBus(&ADCD1);

	if (samples_buf[0] < 2048 || samples_buf[1] < 2048)
		return false;
	else
		return true;
}

void
bot_lee_tcrt1000(uint8_t s[])
{
	adcAcquireBus(&ADCD1);
	adcConvert(&ADCD1, &adccg, samples_buf, ADC_BUF_DEPTH);
	adcReleaseBus(&ADCD1);

	s[0] = samples_buf[0] >> 4;	// * 256 / 4096
	s[1] = samples_buf[1] >> 4;
}

#endif
