#include "bot.h"
#if BOT_PLATFORM == 2015

#include <avr/io.h>

void
bot_setup(void)
{
	/* Resumen de configuración de GPIOs en el Atmega328p:
	 *
	 * - Los registros son: DDRx, PORTx y PINx.
	 * - Los bits son: DDxn, PORTxn y PINxn.
	 *
	 * DDRx: dirección (0 = entrada, 1 = salida)
	 * Si entrada:
	 *   PORTx: pull up (0 = no, alta impedancia, 1 = pull up)
	 * Si salida:
	 *   PORTx: salida (0 = bajo, 1 = alto)
	 *
	 * Lectura de PINx: lectura del pin (independiente de DDRx)
	 * Escritura en PINx: alterna el valor de PORTx (independiente de DDRx)
	 *
	 * Notar que en los pasos de DDR/PORT de 00 a 11 y viceversa hay que pasar
	 * por 01 y 10 en ambos casos. Las posibles combinaciones son:
	 *
	 * DDR/PORT:
	 * - 00 (in, high z) -> 01 (in, pull-up) -> 11 (out, high)
	 * - 00 (in, high z) -> 10 (out, low)    -> 11 (out, high)
	 * - 11 (out, high)  -> 01 (in, pull-up) -> 00 (in, high z)
	 * - 11 (out, high)  -> 10 (out, low)    -> 00 (in, high z)
	 */

	///////////////////////////////////////////////////////////////////////////
	// Serial
	///////////////////////////////////////////////////////////////////////////

	sdStart(&SD1, &uartCfg);

	///////////////////////////////////////////////////////////////////////////
	// ADC
	///////////////////////////////////////////////////////////////////////////

	// Activa las interrupciones del ADC (ADIE) y pone los prescalers al
	// maximo para obtener la maxima precision. Notar que no enciende
	// inmediatamente (ADEN) el ADC, lo encenderemos a mano cada vez que
	// queramos hacer una conversion para ahorrar energia, a costa de que
	// la primera conversion tarde un poco mas. ADIE se usa para poder
	// dormir el AVR durante la conversion y ahorrar energia. Una ISR vacía
	// se encarga de despertarlo.
	ADCSRA = _BV(ADIE) | _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);

	// Desconecta los buffers digitales de las patas analogicas (p.266),
	// esto no es imprescindible pero reduce el consumo. ADC6 y ADC7 no
	// tienen entrada digital, así que no hacen falta.
	DIDR0 = _BV(ADC0D) | _BV(ADC1D) | _BV(ADC2D) | _BV(ADC3D) | _BV(ADC4D) |
			_BV(ADC5D);

	// Pone todas las entradas analogicas en alta impedancia, excepto ADC6
	// y ADC7 que no hace falta (y ademas no se acceden desde PORTC)
	PORTC &= ~0x0f;
	DDRC  &= ~0x0f;

	// Enciende el ADC, normalmente cada vez que queramos leer los sensores.
	// Podemos apagar el ADC con esto para reducir consumo:
	// ADCSRA &= ~_BV(ADEN);
	ADCSRA |= _BV(ADEN);

	///////////////////////////////////////////////////////////////////////////
	// TCRT1000 frontales
	///////////////////////////////////////////////////////////////////////////

	PORTD &= ~_BV(PORTD4);	// No pull up
	DDRD &= ~_BV(DDD4);		// Input

	///////////////////////////////////////////////////////////////////////////
	// MOTORES
	///////////////////////////////////////////////////////////////////////////

	// Configura los 4 pines de los motores como salidas:
	// Motor 1: PD6 (M1, OC0A), PD5 (M1, OC0B)
	// Motor 2: PB3 (M2, OC2A), PD3 (M2, OC2B)
	DDRD |= (_BV(3) | _BV(5) | _BV(6));
	DDRB |= _BV(3);

	// Y Los pone en estado alto (frenar los motores)
	PORTD |= (_BV(3) | _BV(5) | _BV(6));
	PORTB |= _BV(3);

	///////////////////////////////////////////////////////////////////////////
	// TIMERS Y PWM
	///////////////////////////////////////////////////////////////////////////

	// Configura los timers 0 y 2 en modo fast pwm (COM0x=3) invertido (WGM=3)

	TCCR0A = _BV(COM0A1) | _BV(COM0A0) | _BV(COM0B1) | _BV(COM0B0) |
			_BV(WGM01) | _BV(WGM00);
	TCCR2A = _BV(COM2A1) | _BV(COM2A0) | _BV(COM2B1) | _BV(COM2B0) |
			_BV(WGM21) | _BV(WGM20);

	// Usa como fuente clkIO / 256 (20 MHz / 256 / 256 = 305 Hz)
	TCCR0B = _BV(CS02);
	TCCR2B = _BV(CS22) | _BV(CS21);

	// Frena los motores
	OCR0A = OCR0B = OCR2A = OCR2B = 0;

	///////////////////////////////////////////////////////////////////////////
	// AHORRO DE ENERGÍA
	///////////////////////////////////////////////////////////////////////////

	// Idle mode is the only mode that will keep the UART running
	SMCR = _BV(SE);     // enable sleep instruction, idle mode

	///////////////////////////////////////////////////////////////////////////
	// SONAR
	///////////////////////////////////////////////////////////////////////////

	bot_reset_sonar_pin();
	PCICR |= _BV(PCIE0);	// pin change interrupt enable 0 (PCINT0..7)
	PCMSK0 |= _BV(PCINT1);	// pin change mask register 0 (PCINT0..7)

	///////////////////////////////////////////////////////////////////////////
	// LED de la placa
	///////////////////////////////////////////////////////////////////////////

	DDRB |= _BV(DDB5);		// PB5 now an output
	PORTB &= ~_BV(PORTB5);	// no pull-up (might be enabled by board.h)
	bot_led(0);				// turn off on-board led

	///////////////////////////////////////////////////////////////////////////
	// MPU INT
	///////////////////////////////////////////////////////////////////////////

	DDRD &= ~(1 << DDD2);	// PD2 (PCINT0 pin) is now an input
	PORTD |= (1 << PORTD2);	// turn On the Pull-up

	EICRA |= (1 << ISC00) | (1 << ISC01);    // set INT0 to trigger on RAISING edge
	EIMSK |= (1 << INT0);     // Turns on INT0
}

#endif
